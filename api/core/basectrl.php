<?php
/*
* dalam unit ini adalah kelas Post, Get, Session
* serta kelas BaseCtrl (main controller)
*/

/* get, post dan session dijadikan kelas sendiri */
class Post{
    protected $_vars=array();

    function __construct(){
        foreach ($_POST as $key => $value) {
            $this->$key=$value;
            $this->_vars[$key]=$value;
        }
        unset($_POST);
    }

    function all(){
        return $this->_vars;
    }

    function key($key){
        if(isset($this->_vars[$key])) return  $this->_vars[$key];
        return false;
    }

    /* manipulasi $_POST[$key] */
    function set($key,$val){
        $this->_vars[$key]=$val;
    }

    function delete($key){
        unset($this->_vars[$key]);
    }
    function clear(){
        $this->_vars=array();
    }
}

class Get{
    protected $_vars=array();
    function __construct(){
        foreach ($_GET as $key => $value) {
            $this->$key=$value;
            $this->_vars[$key]=$value;
        }
        unset($_GET);
    }
    function all(){
        return $this->_vars;
    }
    function key($key){
        if(!empty($this->_vars[$key])) return $this->_vars[$key];
        return false;
    }
    /* manipulasi $_GET[$key] */
    function set($key,$val){
        $this->_vars[$key]=$val;
    }
    function delete($key){
        unset($this->_vars[$key]);
    }
    function clear(){
        $this->_vars=array();
    }
}
/* kelas untuk session */
class Session{
    function __construct(){
        if(session_id()==false){
            /* Session 6 jam */
            $lifetime=60*60*6;
            session_set_cookie_params($lifetime);
            session_start();
        }
    }

    /* mengambil session */
    function key($key){
        return isset($_SESSION[$key]) ? $_SESSION[$key]:false;
    }

    /* menyimpan session */
    function set($key,$val=null){
        if(empty($val)) $val=uniqid();
        $_SESSION[$key]=$val;
    }

    /* hapus sebuah variabel dari session */
    function delete($key){
        unset($_SESSION[$key]);
    }

    /* mengambil session_id */
    function id(){
        return session_id();
    }

    /* session id baru */
    function newId(){
        session_regenerate_id();
        return session_id();
    }
    /* menutup sebuah session */
    function close(){
        if(session_id()!= false) session_unset();
    }
}


/*
* Kelas BaseCtrl
* semua controller harap diturunkan dari kelas ini
*/
class BaseCtrl{
    protected $_render=true;

    function __construct(){
        global $query;
        /* init session,post dan get */
        $this->query=$query;
        $this->Post=new Post;
        $this->Get=new Get;
        $this->_status=200;
    }

    /* akhir merender format json variabel $result
    */
    function __destruct(){
        if($this->_status>299) $this->_render=false;
        header('Content-Type: application/json');
        if($this->_render) echo json_encode($this->_result);
        http_response_code($this->_status);
    }

    function data($val){
        $this->_result=$val;
    }

    function render($value=null){
        $this->_render=$value==1;
    }

    function status($code){
        $this->_status=$code;
    }
    function addModel($model){
        if(!isset($this->$model)) $this->$model=new $model;
    }

}
