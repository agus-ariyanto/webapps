<?php
/*untuk otentifikasi dipisah profil beda*/
class OlvAuth extends Model{
    protected $alias='auth';
    protected $columns=array(
        'group_id'=>'INT(1) DEFAULT 0',
        'username'=>'VARCHAR(128)',
        'pwd'=>'VARCHAR(48)',
        'status'=>'INT(1) DEFAULT 1',
        'ccid'=>'INT DEFAULT 0',
        'nama'=>'VARCHAR(256)',
        'alamat'=>'VARCHAR(256)',
        'email'=>'VARCHAR(256)',
        'telp'=>'VARCHAR(24)',
    );

    /* pasword admin -> sha1 :d033e22ae348aeb5660fc2140aec35850c4da997
                    md5 :21232f297a57a5a743894a0e4a801fc3
    63	abce	202cb962ac59075b964b07152d234b70	ABC Engineering, CV	UNTUK DEMO as Mitra	-	-	2	1		220000022

    */
    protected $firstdata=array(
        array(
            'username'=>'admin',
            'pwd'=>'21232f297a57a5a743894a0e4a801fc3',
            'group_id'=>1,
        ),
        array(
            'username'=>'abce',
            'pwd'=>'202cb962ac59075b964b07152d234b70',
            'email'=>'CV UNTUK DEMO as Mitra',
            'group_id'=>'2',
            'ccid'=>'200003686'
        ),
        array(
            'username'=>'ab.not.ce',
            'pwd'=>'202cb962ac59075b964b07152d234b70',
            'email'=>'CV UNTUK DEMO as Mitra Unoficial',
            'group_id'=>'2',
            'ccid'=>'221000122'
        ),
        array(
            'username'=>'sc',
            'pwd'=>'202cb962ac59075b964b07152d234b70',
            'email'=>'softcopy',
            'group_id'=>'3',
        ),
        array(
            'username'=>'hc',
            'pwd'=>'202cb962ac59075b964b07152d234b70',
            'email'=>'hardcopy',
            'group_id'=>'4',
        ),
    );

    function check($username,$pwd){
        if(empty($username)) $username=uniqid();
        if(empty($pwd)) $pwd=uniqid();
        $md5=md5($pwd);
        $this->andWhere('username',$username);
        $this->andWhere('pwd',$md5);
        $this->andWhere('status',1);
        $this->limit(1);
        $res=$this->select();
        if(count($res)>0) return $res[0];
        return 0;
    }
}
