<?php
class Auth extends BaseCtrl{
    function index(){
        $this->data('testing');
    }
    function login(){
        global $secret;
        $this->data(0);
        if($this->Post->key('user')===false||$this->Post->key('pwd')===false) return;
        $this->addModel('OlvAuth');
        $result=$this->OlvAuth->check($this->Post->key('user'),$this->Post->key('pwd'));
        /*
        token pake jwt userdata
        untuk validasi
        */
        if($result!==0){
            unset($result['pwd']);

            $d=new DateTime();
            $result['date']=$d->format('Ymd');
            $token=$d->format('YmdHis');
            $token=sha1($token);
            $result['token']=$token;
            $this->addModel('Session');
            $this->Session->set('token',$token);
            $this->Session->set('group_id',$result['group_id']);
            $data=JWT::encode($result,$secret['code'],$secret['alg']);
            $this->data($data);
            $this->status(200);
            if($this->query[0]==='token') $this->data($token);
        }
    }

    function logout(){
        $this->addModel('Session');
        /*
        dua kali safety
        kasih session id baru, baru di unset
        */
        $this->Session->close();
        $this->data(1);
    }



}
