 <?php
class CommonCtrl extends BaseCtrl{
    protected  $bulan=array(
        '-','Jan','Feb','Mar','Apr','Mei','Juni',
        'Jul','Ags','Sep','Okt','Nov','Des',
    );
    protected $long_bulan=array(
        '-','Januari','Februari','Maret','April','Mei','Juni',
        'Juli','Agustus','September','Oktober','November','Desember'
    );
    protected $hari=array('Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu');

    protected function bulan($idx,$mode=0){
        switch ($mode){
            case 1 : $s=strtoupper($this->bulan[$idx]);
            break;
            case 2 : $s=ucfirst($this->bulan[$idx]);
            break;
            default: $s=$this->bulan[$idx];
        }
        return $s;
    }

    function __construct(){
        global $prefixmodel;
        set_time_limit(0);
        parent::__construct();
        $this->data(0);
        $this->islogin=false;
        $this->model_prefix=$prefixmodel;
    }


    function index(){
      echo $this->indexhtml();
      $this->render(false);
      $this->status(404);
    }

    protected function varJson($json,$jsonkey=array()){
        $var=json_decode($json);
        $return=array();
        foreach($var as $key=>$value){
            $array=array();
            foreach ($jsonkey as $_key=>$_value) $array[$_value]=$value->$_value;
            $return[]=$array;
        }
        return $return;
    }

    protected function convertDate($format=null, $post=null){
        $postdate='';
        if($this->Post->key('tgl')!==false) $postdate=$this->Post->key('tgl');
        if($this->Post->key('tanggal')!==false) $postdate=$this->Post->key('tanggal');
        if(!is_null($post)) $postdate=$post;

        $date=new DateTime($postdate);
        $date->setTimeZone(new DateTimeZone('Asia/Jakarta'));
        $fmt='Ymd';
        if(!empty($format)) $fmt=$format;
        return $date->format($fmt);
    }


    protected function selectList($model,$join=array(),$secjoin=array()){
        $this->addModel($model);
        foreach($join as $value){
            $this->addModel($value);
            $this->$model->join($this->$value);
        }
        /*function secJoin($master,$detail,$detailjoincols='', $mastercol='',$detailcol='')*/
        foreach($secjoin as $value){
            $table=$value['table'];

            $this->addModel($table);
            $this->$model->secJoin($this->$model,$this->$table,$value['column'],$value['join'],$value['join']);
        }
        /*$this->createQuerySelect($this->$model);*/
        return $this->select($model);
    }

    protected function select($model,$join=array(),$child=array()){
        $this->addModel($model);
        $tablemaster=$this->$model->tableName();
        $aliasmaster=$this->$model->tableAlias();
        $master=array();
        $id=false;
        if($this->Post->key('id')!==false) {
            $master[]=$this->$model->select($this->Post->key('id'));

            $qry=$this->$model->testQry();
            $id=true;
            //$count=1;
        }else{

            if($this->Post->key('andwhere')!=false){
                $w=json_decode($this->Post->key('andwhere'));
                foreach($w as $key => $value){
                    $opr='=';
                    if(!empty($value->opr)) $opr=$value->opr;
                    $this->$model->andWhere($value->key,$value->value,$opr);

                }
            }
            if($this->Post->key('orwhere')!=false){
                $w=json_decode($this->Post->key('orwhere'));
                foreach($w as $key => $value){
                    $opr='=';
                    if(!empty($value->opr)) $opr=$value->opr;
                    $this->$model->orWhere($value->key,$value->value,$opr);
                }
            }
            if($this->Post->key('andwherefunc')!=false){
                $w=json_decode($this->Post->key('andwherefunc'));
                    foreach($w as $key => $value){
                        $opr='=';
                        if(!empty($value->opr)) $opr=$value->opr;
                        $this->$model->andWhereFunction($value->key,$value->value,$opr);
                }
            }
            if($this->Post->key('orwherefunc')!=false){
                $w=json_decode($this->Post->key('orwherefunc'));
                    foreach($w as $key => $value){
                        $opr='=';
                        if(!empty($value->opr)) $opr=$value->opr;
                        $this->$model->orWhereFunction($value->key,$value->value,$opr);
                }
            }

            $columns=$this->$model->colNames();

            foreach($columns as $key=>$value){
                if($this->Post->key($key)!==false){
                    $this->$model->andWhere($key,$this->Post->key($key));
                }
            }

            if($this->Post->key('limit')!==false) $this->$model->limit($this->Post->key('limit'));
            if($this->Post->key('from')!==false) $this->$model->from($this->Post->key('from'));
            if($this->Post->key('page')!==false) $this->$model->curPage($this->Post->key('page'));
            if($this->Post->key('order')!==false) $this->$model->orderBy($this->Post->key('order'),0);
            if($this->Post->key('group')!==false) $this->$model->groupBy($this->Post->key('group'));

            /*$this->createQuerySelect($this->$model);*/
            $master=$this->$model->select();
            //file_put_contents(ROOT_DIR.DS.'log'.DS.get_class($this->$model).'-select.txt',$this->$model->testQry());
            //$qry=$this->$model->testQry();
            //$count=$this->$model->countRec();
        }

        foreach($master as $key =>$value){
            foreach($join as $_value ){
                $this->addModel($_value);
                $table=$this->$_value->tableName();
                $alias=$this->$_value->tableAlias();
                $master[$key][$alias]=array();
                if(!empty($value[$alias.'_id'])) $master[$key][$alias]=$this->$_value->select($value[$alias.'_id']);
            }

            foreach($child as $_value){
                $this->addModel($_value);
                $table=$this->$_value->tableName();
                $alias=$this->$_value->tableAlias();
                $this->$_value->andWhere($aliasmaster.'_id',$value['id']);
                $master[$key][$alias]=$this->$_value->select();
            }

        }
        //$master['query']=$qry;
        //$return['count']=$count;
        if($id) return $master[0];
        return $master;
    }

    protected function saveFromJsonPost($postkey,$realname='olv'){
        if($this->Post->key($postkey)==false) return 0;
        $model=ucfirst(strtolower($realname)).ucfirst(strtolower($postkey));
        $this->addModel($model);
        $json=json_decode($postkey);
        if(is_array($json)){
            $ids=array();
            foreach($json as $key=>$value){
                $ids[]=$this->$model->savePost($json[$key]);
            }
            return $ids;
        }
        return $this->$model->savePost($json);
    }

    protected function setupdir($dir){

        $dir=dirname(ROOT_DIR).DS.$dir;
        $array=explode(DS,$dir);
        $s=array_shift($array);

        foreach ($array as $key => $value) {
            $s.=DS.$value;
            if(!is_dir($s)) mkdir($s);
        }
        return $dir;
    }
    protected function indexhtml(){

        /* heredoc whitespace jangan disentuh */
$out=<<<HTML
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Not Found</title>
</head>
<body>
    <h2>404 Not Found</h2>
</body>
</html>
HTML;
        /* akhir whitespace */
        return $out;

    }
    protected function upfile(){
        $upfile=$_FILES['upload'];
        if($upfile['error']==0){
            $dir='';
            $pref='';
            if(!empty($this->query[0])) $dir=$this->query[0];
            if(!empty($this->query[1])) $pref=$this->query[1].'-';
/*            if($this->Post->key('dir')!==false) $dir=$this->Post->key('dir');
            if($this->Post->key('prefix')!==false) $pref=$this->Post->key('prefix').'-';*/
            $ext = strtolower( pathinfo($upfile['name'], PATHINFO_EXTENSION) );
            $file=$pref.$this->convertDate('Ymd-His').'.'.$ext;
            $webdir='upload/'.$dir;
            $dir=strtolower('upload'.DS.$dir);
            $dir=$this->setupdir($dir);
            if(!file_exists($dir.DS.'index.html')) file_put_contents($dir.DS.'index.html',$this->indexhtml());
            if( move_uploaded_file($upfile['tmp_name'], $dir.DS.$file) ) return $webdir.'/'.$file;

        }
        return 0;
    }

}
