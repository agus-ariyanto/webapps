<?php

/*  class dasar untuk session dan memakai otentifikasi login
    semua yang pake login bisa memakai ini */

class SessionCtrl extends SapCtrl{

    function __construct(){
        parent :: __construct();
/*
        $this->Tanggal=new DateTime;
        $this->Tanggal->setTimeZone(new DateTimeZone('Asia/Jakarta'));
*/
        $this->addModel('Session');
        $token=uniqid();
        if($this->Session->key('token')!==false) $token=$this->Session->key('token');
        $this->islogin=$this->Post->key('token') !== false && $token==$this->Post->key('token');

        /* check login */
        /* belum login, hapus semua post
        dan kirim response kode 401 ( no authorized ) */

        if(!$this->islogin){
            $this->Post->clear();
            $this->data(0);
            $this->status(401);
        }
    }

    /*
    generic select
    patern ctrl/model
    misal ?u=pmh/inv    -> controller Pmh
                        -> model OlvInv
    untuk join dan child pakai post
    join/child lebih dari satu pemisah pakai ,

    misal    ?u=pmh/inv
            post ['model'] = inv
            post ['join'] = kat,step
            post ['child'] = pph
    sama dengan olvinv join olvkat join olvstep
    sedangkan childnya olvpph

*/
    function index(){
        if(!$this->islogin) return;
        if(empty($this->query[0])) return;

        $qry=ucfirst(strtolower($this->query[0]));
        $pref=ucfirst(strtolower($this->model_prefix));

        $model=$pref.$qry;
        if(strpos($qry,$pref)!==false && class_exists($qry)) $model=$qry;


        if(!class_exists($model)) return;

        $join=array();
        $array=$this->Post->key('join')!==false ? explode(',',$this->Post->key('join')) : array() ;
        foreach($array as $key=>$value){
            $s=ucfirst($this->model_prefix).ucfirst(strtolower($value));
            if(class_exists($s)) $join[]=$s;
        }

        $child=array();
        $array= $this->Post->key('child')!==false ? explode(',',$this->Post->key('child')) : array() ;
        foreach($array as $key=>$value){
            $s=ucfirst($this->model_prefix).ucfirst(strtolower($value));
            if(class_exists($s)) $child[]=$s;
        }
        $this->data($this->select($model,$join,$child));
    }


    /* upload file, parameter
     * pake query ?u=upload/query-1/query-2
            query-1 : direktori
            query-2 : prefix ( usernamenya mitra )
        misal ?u=upload/inv/abce
                    direktori /upload/inv
                    file abce-[uniq]-[uniq]
        lihat sessionctrl >> upfile dan setupdir
     */
    function upload(){
        echo $this->upfile();
        $this->data('');
        $this->render(false);
    }


    /*
    generic save model
    patern ctrl/save/model
    misal pmh/save/inv  ->controller Pmh
                        ->model OlvInv
    */
    protected function saveData(){
        $model=ucfirst($this->model_prefix).ucfirst(strtolower($this->query[0]));
        if(!class_exists($model)) return 0;
        $this->addModel($model);
        $id=$this->$model->savePost($this->Post->all());
        /*
        return adalah primary id, untuk prevent diconvert string
        di javascript dibersihkan dari non numerik
        */
       // file_put_contents(ROOT_DIR.DS.'log'.DS.uniqid().'-'.get_class($this->$model).'-save.txt',$this->$model->testQry());
        return preg_replace('/[^0-9]/','',$id);

    }
    function save(){
        if(!$this->islogin) return;
        if(empty($this->query[0])) return;
        $this->data($this->saveData());
    }

    /*
    generic post data dengan json stringify untuk multi record
    post parameter 'data' atau query[1]
    */
    function savearray(){
        if(!$this->islogin) return;
        $param='data';
        if(!empty($this->query[1])) $param=$this->query[1];
        $array=json_decode($this->Post->key($param),true);
        $data=array();
        for ($i=0;$i<count($array);$i++) {
            $this->Post->clear();
            foreach ($array[$i] as $key => $value) {
                $this->Post->set($key,$value);
            }
            $data[]=$this->saveData();
        }
        $this->data($data);
    }

}
