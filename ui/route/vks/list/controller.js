define([
  'ui/system/pmh','ui/system/sla','ui/system/helper','ui/system/func'], function() {
    return  ['$scope','$routeParams', '$auth','$window','Pmh',  'Sla', 'Helper', 'Func',
    function( $scope,  $routeParams,   $auth,  $window,   Pmh,   Sla , Helper, Func){

        $scope.navbar={ title:''};
        $scope.step_id=$auth.user.group_id;
        $scope.barcode='';
        $scope.jadwal_hari_ini='';
        $scope.filter_name='';
        $scope.pmh=[];
        $scope.prc=0;
        
        $scope.tabs={}
        Sla.Start()
        .then(function(r){
            var d=r.data.strdate;
            $scope.jadwal_hari_ini=Date.parse(d).toString('dddd, dd/MM/yyyy');
            return Pmh.GetData({id:$scope.step_id},'step');
        })
        .then(function(r){

            $scope.navbar.title='Verifikasi '+r.data.desk;

        })

        $scope.init=function(){
            if($routeParams.prc){
                if($routeParams.prc!='' || $routeParams.prc!=undefined ) $scope.prc=$routeParams.prc;
            }
            $scope.print=0;
            $scope.pmh=[];
            var aw={};
            /*
            prc
            0 : pmh masuk
            1 : pmh telah diproses
            2 : pmh tunda
            3 : pmh masuk > 100 juta
            10: cetak tanda terima
            */

            /* softcopy */

            var aw={}

            /* pmh masuk atau verif ok */
            prc=$scope.prc;

            /* softcopy lihat status proses ( status 2 ) */
            var aw={ status_id:2,step_id:step_id }
            var step_id=$scope.step_id-1;


            if(prc==0||prc==3){

                /* lainnya lihat verifikasi ok ( status 3) */
                if($scope.step_id==4) aw={ status_id:3,step_id:step_id }

                if($scope.step_id>4){
                /*    setelah hardcopy verifikator boleh lompat qc/spv */
                    var a=[
                        {
                            key:'step_id',
                            value:step_id,
                            opr:'>='
                        },
                        {
                            key:'step_id',
                            value:$scope.step_id,
                            opr:'<'
                        }
                    ];

                    /* tabel  100 juta ke bawah */
                    if(prc==0) a.push({
                        key:'dpp',
                        value:'100000000',
                        opr:'<'
                    });

                    /* tabel 100 juta ke atas */
                    if(prc==3) a.push({
                        key:'dpp',
                        value:'100000000',
                        opr:'>='
                    });
                    var s=JSON.stringify(a);
                    aw={status_id:3,andwhere:s}
                }

            }

            /*  permohonan yang telah diproses,
                tampilkan semua kecuali yang ditunda */
            if(prc==1) {
                var a=[
                    {
                        key:'status_id',
                        value:'6',
                        opr:'<>'
                    }

                ];
                var s=JSON.stringify(a);
                aw={ step_id:$scope.step_id,andwhere:s };
            }

            /*  verif permohonan ditunda */
            if(prc==2) aw={ step_id:$scope.step_id, status_id:6 };

            $scope.pmh=[];
            Pmh.GetList(aw).then(function(r){
                $scope.pmh=r.data;
            });
        }

        $scope.toUrl=function(id){
            $window.location.href=alt.baseUrl+'vks/proses?pmh='+id;
        }


        $scope.proses=function(idx){
            var id=$scope.pmh[idx].id;

            /* hanya untuk hardcopy saja */
            if($scope.step_id==4){
                var a=Helper.dateToSAP($scope.pmh[idx].jadwal);
                var b=Helper.dateToSAP($scope.jadwal_hari_ini);
                c=parseInt(a)-parseInt(b);
                if(c>0){

                    /* biar nggak muncul check box prevent bla bla ( cegah bla bla ) */
                    if($window.confirm('Tgl. Hari ini : '+ $scope.jadwal_hari_ini+"\n"+
                    'Tgl. Jadwal : '+$scope.pmh[idx].jadwal+"\n"+
                    'Penjadwalan seharusnya '+c+ " hari lagi \n\n\n"+
                    "Klik Ok untuk tetap diproses \n\n" ) ){
                        $scope.toUrl(id);
                        return true;
                    }
                    return false;
                }
            }
            $scope.toUrl(id);
        }

        /* proses lewat barcode */
        /* nggak pake ini lagi
            ng-keypress="barPress($event)"
         */
        $scope.barPress=function(){
            Pmh.GetData({barcode:$scope.barcode,limit:1},'pmh')
            .then(function(r){
                if(r.data[0].id) $scope.proses(r.data[0]);
            });
        }


        /* export tabel ke xls*/
        $scope.exportToExcel=function(){
            var d=new Date().toString('yyyyMMddhhmmss');
            Helper.exportToExcel('vkslist','vks-'+d);
        }


        $scope.init();

    /* end controller */
    }]
});
