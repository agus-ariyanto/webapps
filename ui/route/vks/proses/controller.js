define([ 'ui/system/sla', 'ui/system/pmh', 'ui/system/helper', 'ui/system/sap', 'ui/system/func' ], function() {
    return ['$scope', '$routeParams', '$auth', '$window', '$timeout', 'Sla', 'Pmh', 'Helper', 'Sap', 'Func',
        function($scope, $routeParams, $auth, $window, $timeout, Sla, Pmh,  Helper, Sap, Func) {

            /* component viewer */
            $scope.view_pogr={};
            $scope.view_faktur={};
            $scope.view_invoice={};
            $scope.view_pdf={};
            $scope.view_pphdenda={};
            $scope.view_sap={};

            /* component editor */
            $scope.form_pph={}
            $scope.form_denda={}
            $scope.form_sap={}

            /* bila dapat diedit */
            $scope.editmode={
                pph:false,
                denda:false,
                sap:false,
                none:true
            }
            $scope.pmh={}

            $scope.show_edit_button=false;


            $scope.status_id=0;

            $scope.awal='';
            $scope.akhir='';
            $scope.jadwal='';
            $scope.group_id=$auth.user.group_id;
            $scope.navbar={
                title:''
            }

/*            $scope.status=[
                {
                    id:3,
                    desk:'Ok',
                },
                {
                    id:4,
                    desk:'Revisi',
                },
                {
                    id:5,
                    desk:'Ditolak',
                },
                {
                    id:6,
                    desk:'Ditunda',
                },
            ];
*/
            $scope.group_id=$auth.user.group_id;
            $scope.subtitle='';

            $scope.status=[];

            /*  nggak pakai $q, promise dilakukan berurutan saja */
            $scope.step=0;



            /* jadwal hardcopy */
            $scope.createJadwal=function(){
                var j=Date.parse($scope.awal);
                j.addDays(2);
                var s=j.toString('yyyy-MM-dd');
                /* check hari libur */
                Sla.CheckLibur({tgl:s}).then(function(r){
                    if(r.data!=0) j.addDays(1);
                    /* check hari sabtu/minggu */
                    var b=j.is().saturday(),bb=j.is().sunday();
                    if(b||bb) j.addDays(2);
                    var ss=j.toString('yyyy-MM-dd');
                    /* check hari libur sekali lagi */
                    Sla.CheckLibur({tgl:ss}).then(function(rr){
                        if(rr.data!=0) j.addDays(1);
                        /* jadwal */
                        $scope.jadwal=j.toString('dddd, dd/MM/yyyy')
                    });
                });
            }

            $scope.viewPDF=function(val){
                if(val==1) $scope.view_pdf.click($scope.pmh.fkt_f);
                if(val==2) $scope.view_pdf.click($scope.pmh.inv_f);
                if(val==3) $scope.view_pdf.click($scope.pmh.ba_f);
                if(val==4) $scope.view_pdf.click($scope.pmh.pogr_f);
                if(val==5) $scope.view_pdf.click($scope.pmh.dpl_f);
            }


            $scope.cancel=function(){
                $window.location.href=alt.baseUrl+'vks/list';
            }

            /* simpan pph */

            $scope.savePph=function(){
                /* edit pmh masukkan total jumlah pph */
                $scope.pmh.totalpph=Func.TotalPph($scope.form_pph.pph);
                //$scope.form_pph.calcTotal();
                var pmh={
                    id:$scope.pmh.id,
                    totalpph:$scope.pmh.totalpph
                }
                return Pmh.SaveData(pmh,'pmh')
                .then(function(){
                    /* simpan perubahan pph */
                    for(var i=0;i<$scope.form_pph.pph.length;i++)
                        $scope.form_pph.pph[i].pmh_id=$scope.pmh.id;


                    var pph=JSON.stringify($scope.form_pph.pph);
                    return Pmh.SaveDataArray({data:pph},'pph','data');
                })
                .then(function(r){
                    var i=0;
                    if(r.data.length<1) return false;
                    for(var i=0;i<r.data.length;i++){
                        if(r.data[i]||r.data[i]!=0||r.data[i]!=undefined) $scope.form_pph.pph[i].id=r.data[i];
                    }
                    return r.data.length>0;
                });
            }

            $scope.saveDenda=function(){
                var pmh={
                    id:$scope.pmh.id,
                    denda:$scope.form_denda.denda,
                    taxcode:$scope.form_denda.taxcode
                };
                return Pmh.SaveData(pmh,'pmh').then(function(r){
                    $scope.pmh.denda=$scope.form_denda.denda;
                    $scope.pmh.taxcode=$scope.form_denda.taxcode;
                    return true;
                });
            }

            $scope.saveSap=function(){
                return Pmh.SaveData($scope.form_sap.sap,'sap');
            }
            

            $scope.saveData=function(){
                if($scope.editmode.pph) $scope.savePph();
                if($scope.editmode.denda) $scope.saveDenda();
                if($scope.editmode.sap) $scope.saveSap();
                $scope.cancelData();
            }

            $scope.cancelData=function(){
                $scope.editmode.pph=false;
                $scope.editmode.denda=false;
                $scope.editmode.sap=false;
                if($scope.group_id==4) $scope.editmode.sap=true;
                $scope.editmode.none=true;
            }

            $scope.gotoEdit=function(val){
                $scope.editmode.pph=false;
                $scope.editmode.denda=false;
                $scope.editmode.sap=false;
                if(val==1) $scope.editmode.pph=true;

                if(val==2) $scope.editmode.denda=true;
                if(val==3) $scope.editmode.sap=true;

                /*hanya untuk mode no edit*/
                if(val==4){
                    if($scope.group_id==4){
                        $scope.gotoEdit(3);
                        return;
                    }
                    $scope.editmode.none=true;
                    return;
                }

                $scope.editmode.none=false;
            }




            $scope.gotoStep=function(val){
                $scope.step=val;
                /*if(val==1) $scope.pogr.init();
                if(val==2) $scope.faktur.init();
                if(val==3) $scope.invoice.init();*/
            }


            $scope.revcode={
                fkt:0,
                inv:0,
                ba:0,
                pogr:0,
                dpl:0
            }

            $scope.submit=function(){
                var d={
                        id:$scope.pmh.id,
                        auth_id:$auth.user.id,
                        status_id:$scope.status_id,
                        step_id:$scope.group_id,
                        catatan:$scope.catatan
                }
                Sla.Stop().then(function(r){
                    /* sla akhir */
                    $scope.akhir=r.data.strdate;
                    var rc= parseInt($scope.revcode.fkt)+
                            parseInt($scope.revcode.inv)+
                            parseInt($scope.revcode.ba)+
                            parseInt($scope.revcode.pogr)+
                            parseInt($scope.revcode.dpl);
                    d.revcode_id=rc;
                    if($scope.group_id==3&&$scope.status_id==3) d.jadwal=$scope.jadwal;
                    return Pmh.SaveData(d,'pmh');
                })
                .then(function(r){
                    d.akhir=$scope.akhir;
                    d.awal=$scope.awal;
                    d.pmh_id=d.id;
                    delete d.id;
                    return Pmh.SaveData(d,'vks');
                })
                .then(function(r){
                    /* status ok simpan data pph,denda dan sap */
                    if($scope.status_id==3){
                        if($scope.group_id==3){
                            $scope.savePph();
                            $scope.saveDenda();
                        }
                        //if($scope.group_id==4) $scope.saveSap();
                    }
                    $scope.cancel();
                });
            }


            Sla.Start()
            .then(function(r){
                $scope.awal=r.data.strdate;
                if($scope.group_id==2) {
                    Func.gotoLogin();return;
                }
                if($scope.group_id==3) $scope.createJadwal();
                return Pmh.GetData({id:$scope.group_id},'step');
            })
            .then(function(r){
                $scope.navbar.title='Proses Verifikasi '+r.data.desk;
                /* untuk verif hanya kode status 3 keatas */
                var s=JSON.stringify([{key:'id',value:2,opr:'>'}]);
                return Pmh.GetData({order:'id',andwhere:s},'OlvStatus');
            })
            .then(function(r){
                $scope.status=r.data;
                return Pmh.GetData({id:$routeParams.pmh}, 'pmh');
            })
            .then(function(r){
                $scope.pmh=r.data;
                if($scope.group_id==3){
                    $scope.editmode.pph=true;
                    $scope.editmode.denda=true;
                }
                if($scope.group_id==4) $scope.editmode.sap=true;
                $scope.editmode.none=true;
                $scope.step=0;
            });


            /* end controller */
    }]
});
