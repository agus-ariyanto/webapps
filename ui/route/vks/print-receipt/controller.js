define([
  'ui/system/pmh','ui/system/sla','ui/system/helper'], function() {
    return  ['$scope','$routeParams', '$auth','$window',  '$timeout', 'Pmh',  'Sla', 'Helper',
    function( $scope,  $routeParams,   $auth,  $window, $timeout,   Pmh,   Sla , Helper){

        $scope.tabs={}
        $scope.navbar={title:'Verifikasi'}
        $scope.step_id=$auth.user.group_id;
        $routeParams.prc=5;
        $scope.vendors=[];
        $scope.pmh=[];
        $scope.incl_inv=true;
        $scope.cetak={
            tgl:'',
            vendor:'',
            po:'',
            inv:'',
            catatan:'',
            from:'',
            to:''
        }

        Sla.Start()
        .then(function(r){
            $scope.cetak.tgl=Date.parse(r.data.strdate).toString('dddd, dd/MM/yyyy');
            return Pmh.GetList({ step_id:$auth.user.group_id, status_id:3,group:'ccid'});
        })
        .then(function(r){
            $scope.vendors=r.data;
            return Pmh.GetData({id:$scope.step_id},'step');
        })
        .then(function(r){
            $scope.navbar.title='Verifikasi '+r.data.desk;
        });


        $scope.selected_ccid='';
        $scope.selectVendor=function(cc_id){
            if(!cc_id) return;
            Pmh.GetList({
                step_id:$auth.user.group_id,
                status_id:3,
                ccid:cc_id
            }).then(function(r){
                $scope.pmh=[];
                $scope.check_all=true;
                angular.forEach(r.data, function(val){
                    val.selected=true;
                    $scope.pmh.push(val);
                });
                if($scope.pmh.length>0) $scope.cetak.vendor=$scope.pmh[0].faktur_nama;
            });
        }


        $scope.check_all=true;
        $scope.checkUncheckAll=function(){
            for(var i=0;i<$scope.pmh.length;i++){
                $scope.pmh[i].selected=$scope.check_all;
            }
        }


        $scope.print=function(){
            if($scope.pmh.length<1) return;
            var inv=[],po=[];
            angular.forEach($scope.pmh, function(val){
                if(val.selected){
                    inv.push(val.invoice_no);
                    po.push(val.po_po);
                }
            });
            $scope.cetak.inv=inv.join(',');
            $scope.cetak.po=po.join(',');
            $timeout(function(){
                $window.print();
            },0);

        }


    /* end controller */
    }]
});
