define([
  'ui/system/pmh','ui/system/sla','ui/system/helper','ui/system/func'], function() {
    return  ['$scope','$routeParams', '$auth','$window','$timeout', 'Pmh',  'Sla', 'Helper', 'Func',
    function( $scope,  $routeParams,   $auth,  $window, $timeout,   Pmh,   Sla , Helper, Func){

        $scope.navbar={ title:'Cetak Label'};
        $scope.step_id=$auth.user.group_id;
        $scope.tabs={}
        $routeParams.prc=4;
        $scope.pmh_list=[];
        $scope.pmh={ barcode:'0001' }
        $scope.step_id=$auth.user.group_id;

        Pmh.GetList({ status_id:3,step_id:$scope.step_id })
        .then(function(r){
            $scope.pmh_list=r.data;
            return Pmh.GetData({id:$scope.step_id},'step');
        })
        .then(function(r){
            $scope.navbar.title='Verifikasi '+r.data.desk;
        });


        var searchAyatPasal=function(val){
            if(val==''||val==undefined) return ' ';
            var a='', b=val.split(' ');
            for(var i=0;i<b.length-1;i++){
                var s=b[i];

                if(s.toUpperCase()=='PASAL') a+='Pasal '+b[i+1];
                if(s.toUpperCase()=='AYAT') a+='Ayat '+b[i+1];

            }
            return a;
        }

        /* barcode */
        $scope.bc = {
            format: 'CODE128',
            height: 40,
            lineColor: '#000000',
            width: 1,
            displayValue: true,
            textAlign: 'center',
            textPosition: 'bottom',
            textMargin: 2,
            fontSize: 12,
        }

        $scope.$watch('bc', function () {
            for (var key in $scope.bc) {
                if ($scope.bc.hasOwnProperty(key)) {
                    switch (typeof $scope.bc[key]) {
                        case 'number':
                        if ($scope.bc[key] == null) {
                            delete $scope.bc[key];
                        }
                        break;
                        case 'string':
                        if ($scope.bc[key].length < 1) {
                            delete $scope.bc[key];
                        }
                        break;
                        case 'boolean':
                        break;
                    }
                }
            }
        }, true);

        $scope.print=function(pmh_id){
            Pmh.GetData({id:pmh_id,
            join:'invoice,faktur,po,sap,auth',
            child:'pph'},'pmh').then(function(r){
                $scope.pmh=r.data;

                for(var i=0;i<$scope.pmh.pph.length;i++){
                    $scope.pmh.pph[i].catatan=searchAyatPasal($scope.pmh.pph[i].ket);

                }
                var denda=parseFloat($scope.pmh.denda);
                var dpp=Helper.toNumber($scope.pmh.dpp);
                var dn=Math.round(dpp*denda/100);
                $scope.pmh.denda_nominal=Helper.toCurrency(dn);
                $scope.pmh.dpp=Helper.toCurrency(dpp);

                $timeout(function(){
                    $window.print();
                });
            });

        }

        /*end controller*/
    }]
});
