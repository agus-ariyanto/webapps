define(['ui/system/func'], function() {
    return  ['$scope','$auth','$window','Func',
    function( $scope,  $auth,  $window,  Func){

      $scope.navbar={title:'Administrator'}
      $scope.form_quota={}
      $scope.form_libur={}
      $scope.form_rek ={}
      $scope.form_po ={}
      $scope.form_gr ={}
      $scope.form_user={}
      $scope.form_check={}

      $scope.tab=0;
      $scope.toTab=function(val){
         $scope.tab=val;
      }
      $scope.init=function(){
          if($auth.user.group_id!=10){
              $window.alert('Silahkan login dengan hak akses Administrator');
              Func.gotoLogin();
              return;
          }
      }
      $scope.init();

  }]
});
