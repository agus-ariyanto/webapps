define(['ui/system/auth'], function(){
    return ['$scope','$validate', '$auth', 'System_Auth','jwtHelper',
        function($scope, $validate, $auth, System_Auth,jwtHelper){

          $scope.data = {user: '', pwd: ''};
          $scope.filter_text=[];
//           fungsi login
          $scope.login = function(){
             var isvalid = $validate()
                .rule($validate.required($scope.data.user), 'Isi username terlebih dahulu!')
                .rule($validate.required($scope.data.pwd), 'Isi password terlebih dahulu!')
                .check();
            if(!isvalid) return;

//             kirim ke server
            /*var j=jwtHelper.*/
            System_Auth.login($scope.data).then(function(response){
               if(response.data==0 || response.data=='' || response.data==undefined) return;
               $auth.login(response.data);
               var group_id=$auth.user.group_id, url='admin';
               if(group_id==2) url='mitra/list';
               if(group_id>2) url='vks/list';
               if(group_id>7) url='cm/list';
               if(group_id>9) url='admin';

               window.location.href = alt.baseUrl + url;

             });
         };
     }];
 });
