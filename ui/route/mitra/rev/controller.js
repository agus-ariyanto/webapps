define(['ui/system/sla', 'ui/system/pmh', 'ui/system/helper', 'ui/system/func'],
    function() {
        return ['$scope', '$routeParams', '$auth', '$window', 'Sla', 'Pmh', 'Helper', 'Func',function($scope, $routeParams, $auth, $window, Sla, Pmh, Helper, Func){

        /* component editor */

        $scope.form_faktur={};
        $scope.form_invoice={};
        $scope.form_dpl={};

        $scope.revisi={
            fkt:false,
            inv:false,
            ba:false,
            pogr:false,
            dpl:false
        }
        $scope.passed={
            fkt:true,
            inv:true,
            ba:true,
            pogr:true,
            dpl:true
        }

        $scope.modified={
            fkt:false,
            inv:false,
            ba:false,
            pogr:false,
            dpl:false
        }

        $scope.pmh={}

        /*title*/
        $scope.navbar = {
            title: "Revisi Pembayaran Invoice"
        };


        /*langkah selanjutnya*/
        $scope.gotoStep=function(val){
            $scope.step=val;
        }

        $scope.ok=function(){
            /* revisi jadi false bila modified untuk penanda revisi passed */

            /* faktur */
            if($scope.form_faktur.modified) $scope.passed.fkt=true;
            if($scope.form_invoice.modified) $scope.passed.inv=true;
            if($scope.form_dpl.modified.ba) $scope.passed.ba=true;
            if($scope.form_dpl.modified.pogr) $scope.passed.pogr=true;
            if($scope.form_dpl.modified.dpl) $scope.passed.dpl=true;

            $scope.gotoStep(0);
        }

        $scope.cancel=function(){
            Func.gotoList();
        }


        $scope.submit=function(){

            if(!$scope.passed.fkt || !$scope.passed.inv || !$scope.passed.ba || !$scope.passed.pogr || !$scope.passed.dpl) {
                    $window.alert('Revisi belum selesai\ntidak bisa dikirim');
                    return;
                }

            var rev=parseInt($scope.pmh.revisi)+1;
            var pmh={
                id:$scope.pmh.id,
                step_id:2,
                status_id:2,
                revisi:rev
            };
            $scope.save(pmh);
        }

        /* belum diimplementasikan */
/*        $scope.draft=function(){
            var pmh={
                id:$scope.pmh.id,
                step_id:1,
            };
            $scope.save(pmh);
        }*/

        $scope.save=function(pmh){
            Pmh.SaveData(pmh,'pmh')
            .then(function(r){
                if($scope.modified.fkt){
                    pmh.fkt_f=$scope.form_faktur.fkt_f;
                    return Pmh.SaveData($scope.form_faktur.fkt,'faktur');
                }
                return $scope.form_faktur.fkt.id;
            })
            .then(function(r){
                if($scope.modified.inv){
                    pmh.inv_f=$scope.form_faktur.inv_f;
                    return Pmh.SaveData($scope.form_invoice.inv,'invoice');
                }
                return  $scope.form_invoice.inv.id;
            })
            .then(function(r){
                if($scope.modified.ba) pmh.ba_f=$scope.form_dpl.ba_f;
                if($scope.modified.pogr) pmh.pogr_f=$scope.form_dpl.pogr_f;
                if($scope.modified.dpl) pmh.dpl_f=$scope.form_dpl.dpl_f;
                return Pmh.SaveData(pmh,'pmh')
            })
            .then(function(r){
                Func.gotoList();
            });
        }

        /*inisialisasi*/
        $scope.init=function(){

            /*prevent error yang coba ngetik url */
            if(!$routeParams.pmh || $routeParams.pmh==''||$routeParams.pmh==undefined) {
                Func.gotoList();
                return;
            }
            /*check apakah quota terbuka */
            Sla.CheckQuota()
            .then(function(r){
                if(r.data.close!=0) {
                    $window.alert(r.data.catatan);
                    Func.gotoList();
                    return;
                }
                /*check pmh dengan akun pemohon*/
                return Func.Check($routeParams.pmh,$auth.user.ccid);
            })
            .then(function(r){
                if(r.data==0){
                    $window.alert('Permohonan tidak bisa diproses,\nSilahkan masuk dengan akun pemohon');
                    Func.gotoLogin();
                    return;
                }
                return Pmh.GetData({id:$routeParams.pmh},'pmh');
            })
            .then(function(r){
                $scope.pmh=r.data;
                $scope.revisi=Func.RevCode($scope.pmh.revcode_id);

                if($scope.revisi.fkt) $scope.passed.fkt=false;
                if($scope.revisi.inv) $scope.passed.inv=false;
                if($scope.revisi.ba) $scope.passed.ba=false;
                if($scope.revisi.pogr) $scope.passed.pogr=false;
                if($scope.revisi.dpl) $scope.passed.dpr=false;

                /*pass semua langsung masuk form invoice*/
                $scope.step=0;
            });
        }

        $scope.init();
        /* end controller */
    }]
});
