
define(['ui/assets/js/Chart.bundle.min','ui/system/pmh','ui/system/sla'],
function(Chart) {
  return ['$scope','$auth','$window','Sla', 'Pmh', function($scope, $auth, $window, Sla, Pmh){

/*
pertahun
?u=sla/year
[
  {
    "kode": 1,
    "bln": "Jan",
    "bulan": "Januari",
    "record": "0"
  },
  {
    "kode": 2,
    "bln": "Feb",
    "bulan": "Februari",
    "record": "0"
  },
  {
    "kode": 3,
    "bln": "Mar",
    "bulan": "Maret",
    "record": "0"
  },
  {
    "kode": 4,
    "bln": "Apr",
    "bulan": "April",
    "record": "0"
  },
  {
    "kode": 5,
    "bln": "Mei",
    "bulan": "Mei",
    "record": "0"
  },
  {
    "kode": 6,
    "bln": "Juni",
    "bulan": "Juni",
    "record": "0"
  },
  {
    "kode": 7,
    "bln": "Jul",
    "bulan": "Juli",
    "record": "0"
  },
  {
    "kode": 8,
    "bln": "Ags",
    "bulan": "Agustus",
    "record": "0"
  },
  {
    "kode": 9,
    "bln": "Sep",
    "bulan": "September",
    "record": "0"
  },
  {
    "kode": 10,
    "bln": "Okt",
    "bulan": "Oktober",
    "record": "0"
  },
  {
    "kode": 11,
    "bln": "Nov",
    "bulan": "November",
    "record": "0"
  },
  {
    "kode": 12,
    "bln": "Des",
    "bulan": "Desember",
    "record": "0"
  }
]


perbulan----
?u=sla/month
[
  {
    "id": "1",
    "desk": "Proses Verifikasi",
    "record": "0"
  },
  {
    "id": "2",
    "desk": "Verifikasi OK",
    "record": "0"
  },
  {
    "id": "3",
    "desk": "Tunda",
    "record": "0"
  },
  {
    "id": "4",
    "desk": "Revisi",
    "record": "0"
  },
  {
    "id": "5",
    "desk": "Draft Permohonan",
    "record": "0"
  },
  {
    "id": "6",
    "desk": "Permohonan ditolak",
    "record": "0"
  }
]
*/
    $scope.init=function(){

        if($auth.user.group_id!=2){
            $window.alert('Silahkan melakukan login dengan akun mitra');
            $window.location.href='auth/login';
            return false;
        }
        Sla.Start({format:'Y'}).then(function(r)){
            var thn=r.data.strdate;
            Pmh.GetData({ccid:})
        }

    }

    $scope.navbar={title:$auth.user.nama};
    $scope.tahun='';
    $scope.bulan='';
    $scope.perbulan_tahun='';
    $scope.bulans=[];

    $scope.openPertahun=function(){

      var thn={ccid:$auth.user.ccid};
      if($scope.tahun!='') thn={tahun:$scope.tahun,ccid:$auth.user.ccid};
      Pmh
      Sla.YearPMH(thn).then(function(r){
        var b=r.data.shortbln;
        var bln_nama=b.split(',');
        bln_nama.slice(0);
        var bln_data=[];
        angular.forEach(bln_nama, function(val){
          bln_data.push(r.data[val]);
        });

        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: bln_nama,
            datasets: [{
              label: 'Permohonan dalam Setahun',
              data: bln_data,
              backgroundColor: [
                '#ad0101','#c99b24','#f1a12a','#dade29','#71cb2b','#18a81e',
                '#27ab7f','#21689c','#556afc','#5537a1','#ad17fe','#cf2b8d'
              ],
              borderColor: [
                '#820000','#9e791a','#986315','#a6aa1c','#549c1c','#118316',
                '#166b4f','#1a4d73','#3543a6','#3b2671','#8613c4','#9c1f69'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            }
          }
        });
      });
    }


    $scope.openPerbulan=function(){
      var bln={cc_id:$auth.userdata.unitid};
      if($scope.bulan!='') bln={bulan:$scope.bulan,cc_id:$auth.userdata.unitid};
      Sla.MonthPMH(bln).then(function(r){
        console.log(r.data);
        $scope.perbulan_tahun=r.data.tahun;
        var b=r.data.longbln;

        var bl=b.split(',');
        bl.slice(0);
        $scope.bulans=[];
        angular.forEach(bl,function(val,key){
          if(val!='-'){
            var bb={ value:key, nama:val };
            $scope.bulans.push(bb);
          }
        });

        var lb_nama=r.data.status.split(',');
        var lb_data=[];
        angular.forEach(lb_nama,function(val){
          lb_data.push(r.data[val]);
        });

        var piectx = document.getElementById("myPieChart").getContext('2d');
        var myChart = new Chart(piectx, {
          type: 'pie',
          data: {
            labels: lb_nama,
            datasets: [{
              label: 'Permohonan Perbulan',
              data: lb_data,
              backgroundColor: [
                '#C8DBCD',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                '#70A294',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            }
          }
        });
      });
    }

    $scope.openPerbulan();
    $scope.openPertahun();






/* end controller */
    }
  ]});
