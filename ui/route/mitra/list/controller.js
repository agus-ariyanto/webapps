define(['ui/system/pmh', 'ui/system/sla',  'ui/system/helper','ui/system/func'], function(){
    return  ['$scope','$routeParams', '$auth','$window', 'Pmh', 'Sla', 'Helper',  'Func',
    function( $scope,  $routeParams,   $auth,  $window,   Pmh,  Sla,  Helper, Func) {

        $scope.navbar={
            title:"Daftar Permohonan Pembayaran Invoice"
        };
        $scope.rev=0;
        $scope.pmh=[];
        $scope.scroll_down=0;
        $scope.quota={
            close:0,
            catatan:''
        }
        $scope.init=function(){
            var s=[
                {key:'step_id',value:'10',opr:'<'},
                {key:'status_id',value:'6',opr:'<'},
                {key:'revisi',value:'0'},
            ];
            /* revisi */
            if($scope.rev==1){
                s=[
                    {key:'revisi',value:'0',opr:'>'},
                    {key:'status_id',value:'6',opr:'<'},
                    {key:'step_id',value:'10',opr:'<'}
                ];
            }
            /* reject */
            if($scope.rev==2){
                s=[
                    {key:'status_id',value:'6'}
                ];
            }
            /* selesai */
            if($scope.rev==3){
                s=[
                    {key:'step_id',value:'10'},
                    {key:'status_id',value:'1'}
                ];
            }
            var qr=JSON.stringify(s);
            Pmh.GetList({andwhere:qr,ccid:$auth.user.ccid},'pmh')
            .then(function(r){
                $scope.pmh=r.data;
                return Sla.CheckQuota();
            })
            .then(function(r){
                $scope.quota=r.data;
                /*Data=Func.DataReset;
                console.log(Data);*/
            });
        }

        $scope.toUrl=function(url,pmh_id){
            var s='';
            if(pmh_id) s='?pmh='+pmh_id;
            $window.location.href=alt.baseUrl+'mitra/'+url+s;
        }



        $scope.gotoBtn=function(rev){
            $scope.rev=rev;
            $scope.init();
        }

        $scope.toTgl=function(val){
            if(val){
                return Date.parse(val).toString('dd/MM/yyyy');
            }
            return ' ';
        }
        $scope.currency=function(val){
            var a=Helper.toNumber(val);
            return Helper.toCurrency(a);
        }
        $scope.calcDenda=function(dpp,denda=0){
            var a=Helper.toNumber(dpp);
            var b=parseFloat(denda);
            return Math.round(a*b/100);
        }
        $scope.dendaCurrency=function(dpp,denda=0){
            var a=$scope.calcDenda(dpp,denda);
            return Helper.toCurrency(a);
        }
        $scope.getTotal=function(dpp,ppn,pph,denda=0){
            var a=Helper.toNumber(dpp);
            var b=$scope.calcDenda(dpp,denda);
            a=parseInt(a);
            var c=a-b-parseInt(pph);
            if(a<=(10000000/1.1)) c+=parseInt(ppn);
            return Helper.toCurrency(c);
        }


        $scope.init();

        /* end controller */
    }]
});
