define(['ui/system/sla', 'ui/system/pmh', 'ui/system/helper', 'ui/system/func'],
    function() {
        return ['$scope', '$routeParams', '$auth', '$window', 'Sla', 'Pmh', 'Helper', 'Func',function($scope, $routeParams, $auth, $window, Sla, Pmh, Helper, Func){

        /* component editor */
        $scope.form_pogr={};
        $scope.form_faktur={};
        $scope.form_invoice={};
        $scope.form_dpl={};

        /*checklist*/
        $scope.passed={
            fkt:false,
            pogr:false,
            inv:false,
            dpl:false
        };

        /*langkah wizard*/
        $scope.step=0;

        $scope.pmh={}
        $scope.pmh.ccid=$auth.user.ccid;
        $scope.intdate='0';
        $scope.pmh.barcode='0000';

        /*title*/
        $scope.navbar = {
            title: "Permohonan Pembayaran Invoice"
        };

        /*untuk barcode*/
        Sla.Start().then(function(r){
            $scope.pmh.awal=r.data.strdate;
            $scope.intdate=r.data.intdate;
        });

        /*langkah selanjutnya*/
        $scope.nextStep=function(){
            var b=false;
            if($scope.step==1){
                b=$scope.form_pogr.check();
                if(b){
                    $scope.pmh.barcode=$scope.form_pogr.po.po.toString()+$scope.intdate.toString();
                    $scope.form_invoice.barcode=$scope.pmh.barcode;
                    $scope.pmh.kat_id=$scope.form_pogr.kat.id;
                    $scope.pmh.po_id=$scope.form_pogr.po.id;
                    $scope.form_faktur.gr=$scope.form_pogr.selected_gr;
                    $scope.form_invoice.inv.gr=Func.TotalGr($scope.form_pogr.selected_gr).join;
                    $scope.form_invoice.inv.po=$scope.form_pogr.po.po;
                    $scope.form_invoice.inv.pekerjaan=$scope.form_pogr.po.ket;
                }
            }
            if($scope.step==2){
                b=$scope.form_faktur.check();
                if(b){
                    var dpp=Helper.toNumber($scope.form_faktur.fkt.dpp);
                    var ppn=Helper.toNumber($scope.form_faktur.fkt.ppn);
                    $scope.form_invoice.inv.faktur=$scope.form_faktur.fkt.no;
                    $scope.pmh.fkt_f=$scope.form_faktur.fkt_f;
                    $scope.form_invoice.inv.dpp=$scope.form_faktur.fkt.dpp;
                    $scope.form_invoice.inv.ppn=$scope.form_faktur.fkt.ppn;
                    $scope.form_invoice.inv.total=Helper.toCurrency(dpp+ppn);
                    $scope.form_invoice.inv.terbilang=Helper.terbilang(dpp+ppn).trim()+' Rupiah';
                }
            }
            if($scope.step==3){
                b=$scope.form_invoice.check();
                if(b){
                    if($scope.form_invoice.inv_f) $scope.pmh.inv_f=$scope.form_invoice.inv_f;
                }
            }
            if(!b){
                $window.alert('Belum bisa lanjut,\nlihat catatan kesalahan');
                return;
            }
            $scope.step+=1;
        }

        /*langkah kembali*/
        $scope.prevStep=function(){
            $scope.step-=1;
        }

        /*checklist*/
        $scope.$watch('step', function(){
            if($scope.pmh.kat_id&&$scope.pmh.po_id) $scope.passed.pogr=true;
            if($scope.pmh.fkt_f) $scope.passed.fkt=true;
            if($scope.pmh.inv_f) $scope.passed.inv=true;
            if($scope.pmh.pogr_f&&$scope.pmh.ba_f&&$scope.dpl_f)$scope.passed.dpl=true;
        });

        /*kembali ke daftar*/
        $scope.cancel=function(){
            Func.gotoList();
        }

        $scope.checkfile=function(){
            var a=$scope.form_faktur.fkt_f!=='',
                b=$scope.form_invoice.inv_f!=='',
                c=$scope.form_dpl.ba_f!=='',
                d=$scope.form_dpl.pogr_f!=='',
                e=$scope.form_dpl.dpl_f!=='';
            if(a) $scope.pmh.fkt_f=$scope.form_faktur.fkt_f;
            if(b) $scope.pmh.inv_f=$scope.form_invoice.inv_f;
            if(c) $scope.pmh.ba_f=$scope.form_dpl.ba_f;
            if(d) $scope.pmh.pogr_f=$scope.form_dpl.pogr_f;
            if(e) $scope.pmh.dpl_f=$scope.form_dpl.dpl_f;
            return a&&b&&c&&d&&e;
        }

        $scope.submit=function(){
            if(!$scope.checkfile()){
                $window.alert('Belum dapat dikirim,\nmasih terdapat file yang belum diunggah');
                return;
            }
            $scope.pmh.step_id=2;
            $scope.pmh.status_id=2;
            $scope.save();
        }

        /*simpan draft*/
        $scope.draft= function(){
            $scope.checkfile();
            $scope.pmh.step_id=1;
            $scope.pmh.status_id=1;
            $scope.save();
        }


        /*submit permohonan baru*/
        $scope.save=function(){


            /* simpan data faktur */
            Pmh.SaveData($scope.form_faktur.fkt,'faktur')
            .then(function(r){
                $scope.pmh.faktur_id=r.data;
                /* simpan invoice  */
                return Pmh.SaveData($scope.form_invoice.inv,'invoice');
            })
            .then(function(r){
                $scope.pmh.invoice_id=r.data;
                /* simpan pmh */
                $scope.pmh.dpp=Helper.toNumber($scope.form_faktur.fkt.dpp);
                return Pmh.SaveData($scope.pmh,'pmh');
            })
            .then(function(r){
                /*flag gr dengan id pmh */
                angular.forEach($scope.form_pogr.selected_gr, function(val){
                    var gr={
                        id:val.id,
                        pmh_id:r.data
                    }
                    Pmh.SaveData(gr,'gr');
                });
                Func.gotoList();
            });
        }


        /*inisialisasi*/
        $scope.init=function(){

            /*prevent error yang coba ngetik url */
            if($routeParams.pmh) {
                $window.location.href=alt.baseUrl+'mitra/baru';
                return;
            }
            /*check quota terbuka */
            Sla.CheckQuota()
            .then(function(r){
                if(r.data.close!=0) {
                    $window.alert(r.data.catatan);
                    Func.gotoList();
                    return;
                }
            });
            $scope.step=1;
        }

        $scope.step=0;
        $scope.init();
        /* end controller */
    }]
});
