define([ 'ui/system/func', 'ui/system/pmh', 'ui/system/helper'], function() {
    return ['$scope', '$routeParams', '$auth','Func','Pmh','Helper',
        function($scope, $routeParams, $auth,Func,Pmh,Helper ) {

        $scope.navbar={title:'Lihat Dokumen Permohonan'};
        $scope.pogr={}
        $scope.faktur={}
        $scope.invoice={}
        $scope.dpl={}
        $scope.step=0;
        $scope.nextStep=function(){
            $scope.step+=1;
        }

        $scope.prevStep=function(){
            $scope.step-=1;
        }
        $scope.cancel=function(){
            Func.gotoList();
        }
        var init=function(){
            /* check parameter */
            if(!$routeParams.pmh||$routeParams.pmh==''||$routeParams.pmh==undefined) {
                Func.gotoList();
                return false;
            }
            /* check dan ambil data pmh */
            Func.Check($routeParams.pmh,$auth.user.ccid)
            .then(function(r){
                if(r.data==0){
                    $window.alert('Silahkan login dengan akun pemohon');
                    Func.gotoLogin();
                    return false;
                }
                $scope.step=1;
            });
        }

        init();

    /* end controller */
    }]
});
