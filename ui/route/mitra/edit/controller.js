define(['ui/system/sla', 'ui/system/pmh', 'ui/system/helper', 'ui/system/func'],
    function() {
        return ['$scope', '$routeParams', '$auth', '$window', 'Sla', 'Pmh', 'Helper', 'Func',function($scope, $routeParams, $auth, $window, Sla, Pmh, Helper, Func){

        /* component editor */
        $scope.form_pogr={};
        $scope.form_faktur={};
        $scope.form_invoice={};
        $scope.form_dpl={};

        /*checklist*/
        $scope.passed={
            fkt:false,
            pogr:false,
            inv:false,
            dpl:false
        };

        /*langkah wizard*/
        $scope.step=0;

        $scope.pmh={}
        $scope.pmh.ccid=$auth.user.ccid;

        /*title*/
        $scope.navbar = {
            title: "Permohonan Pembayaran Invoice"
        };


        /*langkah selanjutnya dan check perubahan */
        $scope.nextStep=function(){
            var b=true;
            if($scope.step==2 && $scope.form_faktur.modified ){
                b=$scope.form_faktur.check();
                if(b){
                    var dpp=Helper.toNumber($scope.form_faktur.fkt.dpp);
                    var ppn=Helper.toNumber($scope.form_faktur.fkt.ppn);
                    $scope.form_invoice.inv.faktur=$scope.form_faktur.fkt.no;
                    $scope.pmh.fkt_f=$scope.form_faktur.fkt_f;
                    $scope.form_invoice.inv.dpp=$scope.form_faktur.fkt.dpp;
                    $scope.form_invoice.inv.ppn=$scope.form_faktur.fkt.ppn;
                    $scope.form_invoice.inv.total=Helper.toCurrency(dpp+ppn);
                    $scope.form_invoice.inv.terbilang=Helper.terbilang(dpp+ppn).trim()+' Rupiah';
                }
            }
            if($scope.step==3 && $scope.form_invoice.modified){
                b=$scope.form_invoice.check();
            }

            if(!b){
                $window.alert('Belum bisa lanjut,\nlihat catatan kesalahan');
                return;
            }
            $scope.step+=1;
        }

        /*langkah kembali*/
        $scope.prevStep=function(){
            $scope.step-=1;
        }

        /*checklist*/
        $scope.$watch('step', function(){
            if($scope.pmh.kat_id&&$scope.pmh.po_id) $scope.passed.pogr=true;
            if($scope.pmh.fkt_f) $scope.passed.fkt=true;
            if($scope.pmh.inv_f) $scope.passed.inv=true;
            if($scope.pmh.pogr_f&&$scope.pmh.ba_f&&$scope.dpl_f)$scope.passed.dpl=true;
        });

        /*kembali ke daftar*/
        $scope.cancel=function(){
            Func.gotoList();
        }

        $scope.checkfile=function(){
            var a=$scope.form_faktur.fkt_f!=='',
                b=$scope.form_invoice.inv_f!=='',
                c=$scope.form_dpl.ba_f!=='',
                d=$scope.form_dpl.pogr_f!=='',
                e=$scope.form_dpl.dpl_f!=='';
            if(a) $scope.pmh.fkt_f=$scope.form_faktur.fkt_f;
            if(b) $scope.pmh.inv_f=$scope.form_invoice.inv_f;
            if(c) $scope.pmh.ba_f=$scope.form_dpl.ba_f;
            if(d) $scope.pmh.pogr_f=$scope.form_dpl.pogr_f;
            if(e) $scope.pmh.dpl_f=$scope.form_dpl.dpl_f;
            return a&&b&&c&&d&&e;
        }



        $scope.submit=function(){
            if(!$scope.checkfile()){
                $window.alert('Belum dapat dikirim,\nmasih terdapat file yang belum diunggah');
                return;
            }
            $scope.pmh.step_id=2;
            $scope.pmh.status_id=2;
            $scope.save();
        }

        /*simpan draft*/
        $scope.draft=function(){
            $scope.checkfile();
            $scope.pmh.step_id=1;
            $scope.pmh.status_id=1;
            $scope.save();
        }

        /*submit permohonan baru*/
        $scope.save=function(){

            /* simpan data faktur untuk edit proses dibalik */
            Pmh.SaveData($scope.pmh,'pmh')
            .then(function(r){
                if($scope.form_faktur.modified) return Pmh.SaveData($scope.form_faktur.fkt,'faktur');
                return true;
            })
            .then(function(r){
                /* simpan invoice  */
                if($scope.form_invoice.modified) return Pmh.SaveData($scope.form_invoice.inv,'invoice');
                return true;
            })
            .then(function(r){
                Func.gotoList();
            });
        }



        /*inisialisasi*/
        $scope.init=function(){

            /*prevent error yang coba ngetik url */
            if(!$routeParams.pmh || $routeParams.pmh==''||$routeParams.pmh==undefined) {
                Func.gotoList();
                return;
            }
            /*check apakah quota terbuka */
            Sla.CheckQuota()
            .then(function(r){
                if(r.data.close!=0) {
                    $window.alert(r.data.catatan);
                    Func.gotoList();
                    return;
                }
                /*check pmh dengan akun pemohon*/
                return Func.Check($routeParams.pmh,$auth.user.ccid);
            })
            .then(function(r){
                if(r.data==0){
                    $window.alert('Permohonan tidak bisa diproses,\nSilahkan masuk dengan akun pemohon');
                    Func.gotoLogin();
                    return;
                }
                /*pass semua langsung masuk form invoice*/
                $scope.pmh.id=$routeParams.pmh;
                $scope.step=3;
            });
        }

        $scope.step=0;
        $scope.init();
        /* end controller */
    }]
});
