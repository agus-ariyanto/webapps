alt.modules.auth = angular.module('alt-auth', ['angular-jwt'])
    .factory('$auth', ['$log', '$window', 'jwtHelper', function($log, $window, jwtHelper){
        store.set(alt.application + '_token', store.get(alt.application + '_token') || '');
        return {
            token:'',
            user:{},

            login: function(data){
              store.set(alt.application + '_token', data);
              this.user=jwtHelper.decodeToken(data);
              this.token=this.user.token;
            },
            logout: function(){
                this.token='';
                store.set(alt.application + '_token', '');
            },
            islogin: function(){
                return this.token != '' || this.token != 0 || this.token != undefined ;
            }
        };
    }])
    .config(['$provide', '$httpProvider', function($provide, $httpProvider){
        $provide.factory('authHttpInterceptor', ['$auth', '$log', '$q', '$window', function($auth, $log, $q, $window){
            return {
                request: function(config){
                    if(config.url.indexOf(alt.serverUrl) === 0 && config.data && $auth.token) config.data.token = $auth.token;
                    return config;
                }
            };
        }]);

        $httpProvider.interceptors.reverse().push('authHttpInterceptor');
        $httpProvider.interceptors.reverse();
    }])

    .run( ['$auth', '$log', function($auth, $log){
        var token = store.get(alt.application + '_token');
        if(token) $auth.login(token);
    }]);


alt.module('alt-auth', alt.modules.auth);
