alt.application = 'OLIVE';
alt.title = 'PT Indonesia Comnets Plus';
alt.description = 'Online Invoice Verification Service';
alt.environment = 'development';
alt.componentFolder = 'ui/component';
alt.routeFolder = 'ui/route';
alt.version = '0.3.0';


alt.urlArgs = '_v=' + alt.version+'&_t='+Date.today().toString('yyyy.MM.dd');
//alt.serverUrl = 'http://localhost/olive-baru/api/?u=';
//alt.serverUrl = window.location.origin+'/olive-baru/api/?u=';
alt.serverUrl = 'api/?u=';
alt.defaultRoute = 'auth/login';

alt.secure = {};
alt.module('ngSanitize');
alt.module('ui.select');
alt.module('datePicker');
alt.module('720kb.tooltips');
alt.module('angular-barcode');

// set window title
document.title = alt.title + ' :: ' + alt.description;

// advanced configuration
alt.run(['$log', '$q', '$rootScope', '$route', '$window', '$auth', '$alert', '$api', '$timeout',
function($log, $q, $rootScope, $route, $window, $auth, $alert, $api, $timeout){
    $rootScope.$auth = $auth;
    $rootScope.template = 'full';
    $rootScope.$on('$routeChangeStart', function(event, currRoute, prevRoute){
      $rootScope.template = currRoute.params.altcontroller == 'auth' ? 'full' : 'content';
      if(currRoute.params.altcontroller != 'auth'){
        if(!$auth.islogin()) $window.location.href = alt.baseUrl + 'auth/login';
      }


      /*if(alt.environment == 'production' && typeof alt.secure.key === 'undefined')
      $api('system/auth').connect('secure').then(function(response){
        alt.secure = response.data;
      });*/
    });
  }])
  .config(['$provide', '$httpProvider', '$compileProvider',
  function($provide, $httpProvider, $compileProvider){
    $provide.factory('secureHttpInterceptor', ['$auth', '$log', '$q', '$window',
    function($auth, $log, $q, $window){
      return {
        request: function(config){
          /*if(config.data && alt.environment == 'production' && config.method == "POST" && config.url.indexOf(alt.serverUrl) === 0 && config.headers["Content-Type"] == "application/x-www-form-urlencoded"){
            var key = CryptoJS.enc.Base64.parse(alt.secure.key);
            var iv = CryptoJS.enc.Base64.parse(alt.secure.iv);
            var encrypted = CryptoJS.AES.encrypt(
              config.data,
              key,
              { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: iv});
              config.data = encrypted.toString();
            }*/
            return config;
          }
        ,response: function(response){
        /*    if(alt.environment == 'production' && response.config.method == "POST" && response.config.url.indexOf(alt.serverUrl) === 0 && response.config.headers["Content-Type"] == "application/x-www-form-urlencoded" && response.config.url.indexOf("auth/secure") == -1){
              var encrypted = CryptoJS.enc.Base64.parse(response.data);
              var key = CryptoJS.enc.Base64.parse(alt.secure.key);
              var iv = CryptoJS.enc.Base64.parse(alt.secure.iv);
              var decrypted = CryptoJS.enc.Utf8.stringify(CryptoJS.AES.decrypt(
                { ciphertext: encrypted },
                key,
                { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: iv}));

                try{
                  response.data = angular.fromJson(decrypted);
                }catch(e){uiMaskConfigProvider.
                  response.datuiMaskConfigProvider = decrypted;
                }
              }*/
              return response;
            }
          };
        }]);
        $httpProvider.interceptors.push('secureHttpInterceptor');
   }]);
