define([], function(){
    alt.factory('System', ['$log', '$api', '$alert', '$q', '$rootScope', '$loading', function($log, $api, $alert, $q, $rootScope, $loading){
        return function(url, pkey){
            pkey = pkey || '';
            return $api(url, pkey, {
                connect: function(params){
                    $loading.show();
                },
                success: function(response){
                    $loading.close();
                },
                error: function(error, params, deferred, iscancelled){
                    // $loading.close();
                    // if(iscancelled) return true;
                    //
                    // error.message = error.message || (error.data ? error.data.message : 'Error tidak diketahui');
                    // if(typeof error.message === "string"){
                    //     $alert.add(error.message, $alert.danger);
                    // }else if(error.message.length){
                    //     angular.forEach(error.message, function(message){
                    //         $alert.add(message, $alert.danger);
                    //     });
                    // }

                }
            });
        };
    }]);
});
