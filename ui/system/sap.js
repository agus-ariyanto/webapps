define([ 'ui/system/index' ], function(){
    alt.factory('Sap', ['System',function(System){
        var api = System('sap');
/*
    function postatus()
    function parking()
    function miro()
    function fb65()
    function pph()
    function grstatus()
    function po_from_sap()
    function gr_from_sap()
    function vendor_from_sap()
    function clearing_from_sap()
*/
        api.Send=function(data,url){
            return this.connect(url,data);
        }

        return api;
    }]);
});
