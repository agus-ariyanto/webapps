define([ 'ui/system/index','ui/system/helper' ], function(){
    alt.factory('Admin', ['System','$auth','Helper',function(System, $auth, Helper){
        var api = System('admin');
        /* upload file pdf */
        api.Sap = function(data,model){
            model='sap/'+model;
            return this.connect(model, data);
        }

        api.Table = function(data,model){
            model='table/'+model;
            return this.connect(model, data);
        }

        
        return api;
    }]);
});
