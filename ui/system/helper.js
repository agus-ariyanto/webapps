define([], function(){
    alt.factory('Helper', function(){
        var api={};


        var tbl = function(nilai) {
            var angka = ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];
            var x = parseInt(nilai);
            if (x < 12 && !isNaN(x)) return " " + angka[x];
            if (x < 20 && !isNaN(x)) return tbl(x - 10) + " Belas";
            if (x < 100) return tbl(x / 10) + " Puluh" + tbl(x % 10);
            if (x < 200) return " Seratus" + tbl(x - 100);
            if (x < 1000) return tbl(x / 100) + " Ratus" + tbl(x % 100);
            if (x < 2000) return " Seribu" + tbl(x - 1000);
            if (x < 1000000) return tbl(x / 1000) + " Ribu" + tbl(x % 1000);
            if (x < 1000000000) return tbl(x / 1000000) + " Juta" + tbl(x % 1000000);
            return tbl(x / 1000000000) + " Milyar" + tbl(x % 1000000);
            //if (x < 1000000000000000) return tbl(x / 1000000000000) + " Trilyun" + tbl(x / 1000000000) + " Milyar" + tbl(x / 1000000) + " Juta" + tbl(x % 1000000);
        }

        api.getBulan=function(){
           return [
            {val:"0", name:"-"},
            {val:"01", name:"Januari"},
            {val:"02", name:"Februari"},
            {val:"03", name:"Maret"},
            {val:"04", name:"April"},
            {val:"05", name:"Mei"},
            {val:"06", name:"Juni"},
            {val:"07", name:"Juli"},
            {val:"08", name:"Agustus"},
            {val:"09", name:"September"},
            {val:"10", name:"Oktober"},
            {val:"11", name:"November"},
            {val:"12", name:"Desember"}
          ];
        }

        api.extractFileName=function(fullpath){
          return fullpath.replace(/^.*[\\\/]/, '');
        }
        api.toNumber=function(nilai){
          if (nilai==''||nilai==undefined) return 0;
          var v=nilai.toString().replace(/[^0-9]/g,'');
          return parseInt(v);
        }

        api.toCurrency=function(val){
          if(val==''||val==undefined||val==0||val=='0') return '';
          var v=val.toString().replace(/[^0-9]/g,'');
          return v.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        }

        api.terbilang=function(val){
          var v=tbl(val);
          return v;
        }

        /* dimensi object pdf */
        api.getHeight=function(width_in_percent){
          var w,h,o={};
          w=document.getElementById('pdf-object-wrapper').clientWidth;
          w = w * width_in_percent / 100;
          h = 4*w/3;
          o.width=parseInt(w);
          o.height=parseInt(h);
          return o;
        }

        /* tanggal untuk SAP */
        api.dateToSAP=function(val){
          var v=val.replace(/[^0-9]/g,'');
          var y,m,d;
          y=v.substr(4,4);
          m=v.substr(2,2);
          d=v.substr(0,2);
          return y+m+d;
        }

        /* tanggal dari SAP */
        api.sapToDate=function(val){
          var v=val.replace(/[^0-9]/g,'');
          y=v.substr(0,4);
          m=v.substr(4,2);
          d=v.substr(6,2);
          return d+'/'+m+'/'+y;
        }

        /* tanggal dari SAP */
        api.sapToDbDate=function(val){
          var v=val.replace(/[^0-9]/g,'');
          y=v.substr(0,4);
          m=v.substr(4,2);
          d=v.substr(6,2);

          return y+'-'+m+'-'+d;
        }

        /* export table ke excel
         * parameter table = id table
         * style jangan menggunakan css
         * lebih baik pake hidden div sebagai wrapper-nya
         * misal
         * <div style="display:none">
         *    <table id="table-excel">..</table>
         * </div>
         * <button type="button" ng-click="ConvertExcel('table-excel','test')">Convert</button>
         * dalam controller.js
         * $scope.ConvertExcel=function(table,name){
         *    Helper.exportToExcel(table,name);
         * }
         */
        api.exportToExcel=function(table,name){
          var t=document.getElementById(table);
          var base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) };
          var format = function (s) { return s.replace(/{(\w+)}/g,"")};
          var  uri = 'data:application/vnd.ms-excel;base64,',
          template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" '+
                     'xmlns:x="urn:schemas-microsoft-com:office:excel" '+
                     'xmlns="http://www.w3.org/TR/REC-html40" lang="en">'+
                     '<head><meta charset="utf-8"> <meta http-equiv="content-language" content="en"> '+
                     '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>'+
                     '<x:Name>'+name+'</x:Name>'+
                     '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>'+
                     '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>'+
                     '</xml><![endif]--></head><body>'+
                     '<table>'+t.innerHTML+'</table></body></html>';
          /*
          pake method ini bisa
             window.location.href=uri + base64(format(template));

          tapi lebih smooth pake bawah ini,
          nama file bisa diganti dengan easy human readingfile
            - buat element anchor dengan attribut href dan download
            - diklik
            - element dihapus..
          */

          var ref = uri + base64(format(template));
          var a = document.createElement('A'),
              b = document.createAttribute('href'),
              c = document.createAttribute('download');

          b.value=ref;
          c.value=name+'.xls';
          a.setAttributeNode(b);
          a.setAttributeNode(c);
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        }

        api.createPdfObject=function(pdf_file,div_id,percent_width){
            var d = api.getHeight(percent_width);
            var ob= '<object type="application/pdf" data="'
                    +pdf_file+
                    '" width="'+d.width+'" height="'+d.height+
                    '">browser tidak mendukung obyek embed, <br>Silahkan unduh Chrome atau Firefox terbaru</object>';
            document.getElementById(div_id).innerHTML=ob;
            return ob;
        }


        return api;
    });
});
