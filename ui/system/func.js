define(['ui/system/pmh','ui/system/helper'], function(){
    alt.factory('Func', ['$auth','$window','Pmh','Helper',function($auth,$window,Pmh,Helper){
        var api={};

        /*Kosongkan Data*/
        api.DataReset=function(){
            return {
                kat:{},
                po:{},
                gr:[],
                pmh:{
                    fkt_f:'',
                    inv_f:'',
                    ba_f:'',
                    pogr_f:'',
                    dpl_f:'',
                    barcode:'0000'
                },
                fkt:{
                    no :'',
                    nama :'',
                    npwp :'',
                    to_nama :'',
                    to_npwp :'',
                    dpp :'',
                    ppn :''
                },
                inv:{
                    no :'',
                    po :'',
                    gr :'',
                    faktur :'',
                    pekerjaan :'',
                    tgl :'',
                    jatuhtempo :'',
                    jasa :'',
                    material :'',
                    dpp :'',
                    ppn :'',
                    total :'',
                    terbilang :'',
                    bank :'',
                    rek :'',
                    atasnama :'',
                    nama :'',
                    jabatan :''
                },
                pph:[],
                sap:{
                    ccid:'',
                    parking:'',
                    miro:'',
                    tahun:'',
                    fb65:'',
                    fb65man:'',
                    fb65man_amount:'',
                    fb60:'',
                    fb60_amount:'',
                    material:'',
                    material_amount:'',
                    rev:'',
                    catatan:'',
                    tgl:''
                },
                validfaktur:{
                    batas:9090909,
                    pbkk:'PT INDONESIA COMNETS PLUS',
                    nonwapu:'01',
                    wapu:'03',
                    npwp:'01.061.190.3-051.000',
                    nonwaputax:'I2',
                    waputax:'IB'
                },
                revisi:{
                    fkt:false,
                    inv:false,
                    ba:false,
                    pogr:false,
                    dpl:false
                }
            }
        }

        /*hitung total gr yang terpilih*/
        api.TotalGr=function(gr_items){
            var t=0,gr=[];
            angular.forEach(gr_items,function(val){
                if(val.nilai) {
                    t+=Helper.toNumber(val.nilai);
                    gr.push(val.gr);
                }
            });

            var c=Helper.toCurrency(t);
            var j=gr.join(',');
            return {
                number:t,
                currency:c,
                join:j
            }
        }

        /* ditambah currency untuk tampilan pph*/
        api.DataPph=function(data_pph){
            var pph=[];
            for(var i=0;i<data_pph.length;i++){
                var val=data_pph[i];
                val.total='';
                val.nominal_currency='';
                if(val.nominal||val.nominal!==''||val.nominal!==undefined)
                val.nominal_currency=Helper.toCurrency(val.nominal);
                if(val.total||val.total!==''||val.total!==undefined)
                val.total_currency=Helper.toCurrency(val.total);
                pph.push(val);
            }

            return pph;
        }

        api.TotalPph=function(data_pph){
            var total=0;
            angular.forEach(data_pph,function(val){
                if(val.total) total+=val.total;
            });
            var c='';
            if(total>0) c=Helper.toCurrency(total);
            return {
                number:total,
                currency:c
            };
        }


        /*perbedaan total gr dengan dpp*/
        api.DifGrDpp=function(dpp,gr_items){
            var gr=api.TotalGr(gr_items);
            var t=Helper.toNumber(gr.number);
            var d=Helper.toNumber(dpp);
            var m= Math.abs(t.number-d);
            return m;
        }
        /*kode revisi ke angka (revcode_id)*/
        api.CodeRev=function(rev){
            var i=0;
            if(rev.fkt) i+=1;
            if(rev.inv) i+=2;
            if(rev.ba) i+=4;
            if(rev.pogr) i+=8;
            if(rev.dpl) i+=16;
            return i;
        }

        /*angka revcode_id ke kode revisi*/
        api.RevCode=function(rev_id){
            var revisi={
                fkt:false,
                inv:false,
                ba:false,
                pogr:false,
                dpl:false
            };
            var r=parseInt(rev_id);
            if(r>=16){
                revisi.dpl=true;
                r-=16;
            }
            if(r>=8){
                revisi.pogr=true;
                r-=8;
            }
            if(r>=4){
                revisi.ba=true;
                r-=4;
            }
            if(r>=2){
                revisi.inv=true;
                r-=2;
            }
            if(r>=1) revisi.fkt=true;
            return revisi;
        }



        /*cek permohonan dengan akun pemohon*/
        api.Check=function(pmh_id,cc_id){
            return Pmh.GetData({id:pmh_id, ccid:cc_id},'check');
        }

        /*url list mitra*/
        api.gotoList=function(){
            $window.location.href=alt.baseUrl+'mitra/list';
        }

        /*url login*/
        api.gotoLogin=function(){
            $window.location.href=alt.baseUrl+'auth/login';
        }

        return api;

    }]);
});
