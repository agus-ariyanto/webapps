define([ 'ui/system/index' ],
function(){
    alt.factory('Sla', ['System',function(System){
        var api = System('sla');

        api.Start = function(data){
            return this.connect('start',data);
        };

        api.Stop = function(data){
            return this.connect('stop',data);
        };
        api.CheckLibur=function(data){
            return this.connect('checklibur',data);
        }

        api.CheckQuota=function(data){
            return this.connect('checkquota',data);
        }
        api.Year=function(data){
            return this.connect('year',data);
        }
        api.Month=function(data){
            return this.connect('month',data);
        }

        api.GetDate=function(data){
            return this.connect('getdate',data);
        }

        return api;
    }]);
});
