define([ 'ui/system/index','ui/system/helper' ], function(){
    alt.factory('Pmh', ['System','$auth','Helper',function(System, $auth, Helper){
        var api = System('pmh');
        /* upload file pdf */
        api.Upload = function(data,dir){
            dir='upload/'+dir;
            return this.connect(dir, data,{ismultipart: true});
        }

        api.GetData = function(data,model){
            return this.connect(model, data);
        }

        api.SaveData = function(data,model){
            model='save/'+model;
            return this.connect(model, data);
        }
        api.SaveDataArray=function(data,model,postparam){
            model='savearray/'+model+'/'+postparam;
            return this.connect(model, data);
        }

        api.GetList = function(data){
            return this.connect('getlist', data);
        }

        api.Retrieve = function(pmh_id,model){
            return $this.connect('retrieve/'+model,{id:pmh_id});
        }
        return api;
    }]);
});
