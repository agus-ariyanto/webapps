define([
  'ui/system/auth'
], function(){
  return [ '$scope', '$window', '$auth', 'System_Auth',
  function($scope, $window, $auth, System_Auth){
    $scope.userProfile=function(){ }
    $scope.userLogout=function(){
      System_Auth.logout().then(function(response){
        $auth.logout();
        $window.location.href = alt.baseUrl + 'auth/login';
      });
    }

  }];
});
