define([ 'ui/system/pmh','ui/system/helper' ], function(){
  return ['$scope', 'Pmh','Helper', function($scope, Pmh,Helper){

      $scope.libur_list=[];
      $scope.step=0;
      $scope.init=function(){
          var q=[ {key:'tgl',value:'CURDATE()',opr:'>='}];
          var s=JSON.stringify(q);

          Pmh.GetData({andwherefunc:s},'libur')
          .then(function(r){
              $scope.libur_list=r.data;
              $scope.step=0;
          });
      }

      $scope.libur={}

      $scope.save=function(){
          Pmh.SaveData($scope.libur,'libur').then(function(r){
              $scope.libur={
                  tgl:'',
                  ket:''
              }
              $scope.init();
          });
      }

      $scope.open=function(idx){
          $scope.libur={
              tgl:'',
              ket:''
          }
          if(idx!==''||idx!==undefined) $scope.libur=$scope.libur_list[idx];

          $scope.step=1;
      }

      $scope.cancel=function(){
          $scope.step=0;
      }

      $scope.init();

      /*end controller*/
    }]
});
