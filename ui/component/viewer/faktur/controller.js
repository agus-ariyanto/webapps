define(['ui/system/helper','ui/system/pmh'], function() {
    return [ '$scope','$routeParams','Helper','Pmh', function(
        $scope ,$routeParams,Helper,Pmh){

            $scope.fkt={};
            $scope.fkt_f=null;

            Pmh.GetData({id:$routeParams.pmh,join:'faktur'},'pmh')
            .then(function(r){
                $scope.fkt=r.data.faktur;
                if(r.data.fkt_f){
                    $scope.fkt_f=r.data.fkt_f;
                    Helper.createPdfObject($scope.fkt_f,'faktur-object',56);
                }
            });
/* end controller */
    }]
});
