define([ 'ui/system/pmh', 'ui/system/helper'], function() {
    return [ '$scope','$routeParams','$auth','$window', 'Pmh', 'Helper', function( $scope , $routeParams , $auth , $window ,  Pmh , Helper){

        $scope.filepdf={};
        $scope.filepdf.single='';
        $scope.filepdf.one='';
        $scope.filepdf.two='';

        $scope.click=function(val){
            if($scope.filepdf.one===''){
                $scope.filepdf.one=val;
                return;
            }
            $scope.filepdf.two=val;
            if($scope.filepdf.one===$scope.filepdf.two){
                $scope.filepdf.single=val;
                $scope.filepdf.one='';
                $scope.filepdf.two='';
                document.getElementById('pdf-object').innerHTML='<object type="application/pdf" data="'+$scope.filepdf.single+'" class="pdf-fullpage-object"></object>';
                return;
            }

            if($scope.filepdf.two!==''){
                document.getElementById('pdf-object').innerHTML=
                '<object type="application/pdf" data="'+$scope.filepdf.one+'" class="pdf-half-object"></object>'+
                '<object type="application/pdf" data="'+$scope.filepdf.two+'" class="pdf-half-object"></object>';
            }
        }

        $scope.close=function(){
            $scope.filepdf.single='';
            $scope.filepdf.one='';
            $scope.filepdf.two='';
        }

    /* end controller */
    }]
});
