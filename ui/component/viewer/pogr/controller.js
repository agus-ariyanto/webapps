define(['ui/system/helper','ui/system/func','ui/system/pmh'], function() {
    return [ '$scope','$routeParams','Helper','Func','Pmh', function(
              $scope,$routeParams,Helper,Func,Pmh){

        $scope.po={};
        $scope.gr=[];
        $scope.kat={};
        $scope.gr_total='';

        Pmh.GetData({id:$routeParams.pmh,join:'po,kat',child:'gr'},'pmh')
        .then(function(r){
            $scope.po=r.data.po;
            $scope.po.tgl=Helper.sapToDate(r.data.po.tgl);
            $scope.kat=r.data.kat;

            $scope.gr=[];
            angular.forEach(r.data.gr, function(val){
                val.tgl=Helper.sapToDate(val.tgl);
                val.nilai=Helper.toCurrency(val.nilai);
                $scope.gr.push(val);
            });

            $scope.gr_total=Func.TotalGr(r.data.gr).currency;
        });

    /* end controller */
    }]
});
