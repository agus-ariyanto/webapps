define(['ui/system/pmh', 'ui/system/helper'], function() {
    return [ '$scope','$routeParams','Pmh','Helper', function(
        $scope,$routeParams,Pmh,Helper){
        $scope.inv={};
        $scope.inv_f=null;

        Pmh.GetData({id:$routeParams.pmh,join:'invoice'},'pmh')
        .then(function(r){
            $scope.inv=r.data.invoice;
            if(r.data.inv_f) {
                $scope.inv_f=r.data.inv_f;
                Helper.createPdfObject($scope.inv_f,'invoice-object',56);
            }
        });

        /* end controller */
    }]
});
