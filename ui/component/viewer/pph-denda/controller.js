define(['ui/system/pmh','ui/system/sap', 'ui/system/sla', 'ui/system/helper'],function() {
    return [ '$scope', '$routeParams', '$auth','$window', 'Pmh','Sap','Sla', 'Helper',
    function( $scope, $routeParams, $auth, $window, Pmh, Sap, Sla, Helper) {

        /* pph */
        $scope.pph=[];
        $scope.total_pph='';

        /* hitung pph */
        $scope.totalPph=function(pph){
          $scope.pph=[];
          angular.forEach(pph, function(val){
           var persen=parseFloat(val.persen);
           var nominal = 0;
            if(val.nominal) nominal=parseFloat(Helper.toNumber(val.nominal));
            val.nominal_currency=Helper.toCurrency(nominal);
            var total=Math.round(persen*nominal/100);
            val.total_currency='';
            if(total>0) val.total_currency=Helper.toCurrency(total);
            $scope.pph.push(val);
          });
        }

        /* pajak - denda */
        $scope.taxcode='';
        $scope.denda='';
        $scope.denda_nominal='';

        $scope.calcDenda=function(pmh){
            $scope.denda=parseFloat(pmh.denda);
            var dpp=Helper.toNumber(pmh.dpp);
            $scope.denda_nominal=Helper.toCurrency(Math.round($scope.denda*dpp/100));
        }

        Pmh.GetData({id:$routeParams.pmh,child:'pph'},'pmh')
        .then(function(r){
            $scope.totalPph(r.data.pph);
            delete r.data.pph;
            $scope.calcDenda(r.data);
            $scope.taxcode=r.data.taxcode;
        });



    /* end controller */
}]});
