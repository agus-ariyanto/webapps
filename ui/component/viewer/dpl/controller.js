define([ 'ui/system/pmh','ui/system/helper'], function() {
    return [ '$scope','$routeParams','$auth','$window','Pmh','Helper', function( $scope ,$routeParams,$auth,$window,Pmh,Helper){
        $scope.ba_f='';
        $scope.pogr_f='';
        $scope.dpl_f='';
        $scope.setPdfObject=function(){
            if($scope.ba_f && $scope.ba_f!='')
                Helper.createPdfObject($scope.ba_f,'ba-object',32);

            if($scope.dpl_f && $scope.dpl_f!='')
                Helper.createPdfObject($scope.dpl_f,'dpl-object',32);

            if($scope.pogr_f && $scope.pogr_f!='')
                Helper.createPdfObject($scope.pogr_f,'pogr-object',32);
        }
        Pmh.GetData({id:$routeParams.pmh},'pmh')
        .then(function(r){
            if(r.data.ba_f) $scope.ba_f=r.data.ba_f;
            if(r.data.pogr_f) $scope.pogr_f=r.data.pogr_f;
            if(r.data.dpl_f) $scope.dpl_f=r.data.dpl_f;
            $scope.setPdfObject();
        });

  /* end controller */
      }]
});
