define(['ui/system/pmh'], function() {
    return [ '$scope', '$routeParams','Pmh',  function( $scope, $routeParams, Pmh) {

        $scope.sap={}
        Pmh.GetData({id:$routeParams.pmh,join:'sap'},'pmh')
        .then(function(r){
            $scope.sap=r.data.sap;
        });

    }]
});
