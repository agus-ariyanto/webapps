define(['ui/system/pmh','ui/system/func', 'ui/system/helper'], function() {
    return [ '$scope', '$routeParams','Pmh','Func', 'Helper',  function( $scope, $routeParams, Pmh,Func, Helper) {
    $scope.denda='';
    $scope.denda_nominal='';
    $scope.taxcodes=[];
    $scope.taxcode='';
    $scope.pmh={}
    /*$scope.index_ib=0;
    $scope.index_i2=0;*/
    Pmh.GetData({order:'id'},'taxcode').then(function(r){
        $scope.taxcodes=r.data;
    });

    $scope.calcDenda=function(dd=true){
        /*var dpp=parseFloat($scope.pmh.dpp);*/
        var dpp=Helper.toNumber($scope.pmh.invoice.dpp);
        
        if(dd){
            if($scope.denda.length>10) $scope.denda=$scope.denda.toFixed(10);
            var denda=parseFloat($scope.denda);
            $scope.denda_nominal=Helper.toCurrency(Math.round(denda*dpp/100));
            return $scope.denda_nominal;
        }
        var i=$scope.denda_nominal;
        var dn=Helper.toNumber(i);
        var n=dn*100/dpp;
        $scope.denda=n.toFixed(10);
        $scope.denda_nominal=Helper.toCurrency(i);
        return $scope.denda_nominal;
    }


    /*$scope.store=function(){
        Data.pmh.taxcode=$scope.taxcode;

        if($scope.denda) Data.pmh.denda=$scope.denda;
        Data.pmh.taxcode=$scope.taxcode;
    }*/
    Pmh.GetData({},'taxcode')
    .then(function(r){
        $scope.taxcodes=r.data;
        return Pmh.GetData({id:$routeParams.pmh,join:'invoice'},'pmh')
    })
    .then(function(r){
        $scope.denda=r.data.denda;
        $scope.taxcode='IB';
        var vf=Func.DataReset().validfaktur;
        var batas=vf.batas;
        var dpp=r.data.dpp;
        if(dpp<batas) $scope.taxcode=vf.nonwaputax;
        else $scope.taxcode=vf.waputax;
        $scope.pmh=r.data;
        $scope.calcDenda(true);
    });

    /* end controller */
}]});
