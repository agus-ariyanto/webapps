define(['ui/system/sla', 'ui/system/pmh','ui/system/helper','ui/system/func'], function() {
    return [ '$scope','$auth','$routeParams','Sla','Pmh','Helper', 'Func', function( $scope , $auth , $routeParams ,Sla,  Pmh , Helper, Func){

            $scope.fkt={}
            $scope.fkt_f='';
            $scope.error={
                msg:'',
                status:0
            }
            $scope.revisi_fkt=false;
            $scope.gr=[];

            /* flag untuk edit / revisi */
            $scope.modified=false;
            /*
            tanggal mis: 01 Januari 2019
            bulan   mis: Januari
            tahun   mis:2019
            */

            $scope.fakturOnchange=function(el){
                $scope.fkt={ no:'', nama:'', npwp:'', to_nama:'', to_npwp:'', dpp:'', ppn:''};
                var f = el.files[0];
                var  fileReader = new FileReader();
                fileReader.onload=function(){
                    var typedarray = new Uint8Array(this.result);
                    PDFJS.getDocument(typedarray).then(function(pdf) {
                        pdf.getPage(pdf.numPages).then(function(page) {
                            page.getTextContent().then(function(txt) {

                                var txt_items = txt.items;
                                var s = '';
                                for (var i = 0; i < txt_items.length; i++) {
                                    ss = '';
                                    s += txt_items[i].str;
                                }
                                /* cari data faktur pajak ada disini */
                                $scope.fkt.to_nama = s.match(/(?:Nama )(.*?)(?=Alamat)/g)[0].toString().replace('Nama : ', '');
                                $scope.fkt.nama = s.match(/(?:Nama )(.*?)(?=Alamat)/g)[1].toString().replace('Nama : ', '');
                                $scope.fkt.to_npwp = s.match(/(?=NPWP )(.*?)(?=Ko)/g)[0].toString().replace('NPWP : ', '');
                                $scope.fkt.npwp = s.match(/(?=NPWP )(.*?)(?=No)/g)[1].toString().replace('NPWP : ', '');
                                $scope.fkt.no = s.match(/(?=Seri Faktur Pajak : ).*?(Nama)/g).toString().replace('Seri Faktur Pajak : ', '').substr(0, 19);
                                $scope.fkt.dpp = s.match(/(?=Dasar Pengenaan Pajak).*?(,00)/g)[1].toString().replace('Dasar Pengenaan Pajak', '').replace(',00', '');
                                $scope.fkt.ppn = s.match(/(?=x Dasar Pengenaan Pajak).*?(,00)/g).toString().replace('x Dasar Pengenaan Pajak', '').replace(',00', '');
                                /*tambahan tanggal untuk validasi bulan-tahun*/
                                $scope.fkt.tanggal=s.match(/(\, \d{2}) (\D*) (\d{4})/)[0].toString().replace(', ','');


                                $scope.$apply();

                            });

                        });
                    });
                }

                /* upload file immediately */
                Pmh.Upload({upload:f},'fkt/'+$auth.user.username).then( function(r){
                    $scope.fkt_f=r.data;
                    $scope.revisi_fkt=false;
                    $scope.modified=true;
                    $scope.setPdfObject();
                    fileReader.readAsArrayBuffer(f);
                });

            }

            $scope.setPdfObject=function(){
                Helper.createPdfObject($scope.fkt_f,'faktur-object',56);
            }


            Sla.GetDate().then(function(r){
                $scope.validfaktur=Func.DataReset().validfaktur;
                $scope.validfaktur.bulan=r.data.bulan_panjang;
                $scope.validfaktur.tahun=r.data.tahun;
            });

            $scope.check=function(){

                var vf=$scope.validfaktur,
                    dpp=$scope.fkt.dpp,
                    npwp=$scope.fkt.to_npwp,
                    tgl=$scope.fkt.tanggal.split(' '),
                    bulan=tgl[1],
                    tahun=tgl[2],
                    no=$scope.fkt.no.substr(0,2),
                    pbkk=vf.pbkk.replace(/\W/g,'').toUpperCase(),
                    nama=$scope.fkt.to_nama.replace(/\W/g,'').toUpperCase(),
                    dpp_gr=Func.DifGrDpp(dpp,$scope.gr),
                    d=Helper.toNumber(dpp);
                $scope.error={
                    msg:'',
                    status:0
                }
                if(dpp_gr>10){
                    $scope.error.msg+="Selisih DPP dari Faktur Pajak dengan Total nilai GR tidak boleh lebih dari Rp.10 \n";
                    $scope.error.status+=1;
                }
                if(d<=vf.batas && no!=vf.nonwapu){
                    $scope.error.msg+="No. Faktur harus berawalan "+vf.nonwapu+"\n";
                    $scope.error.status+=1;
                }
                if(d>vf.batas && no!=vf.wapu){
                    $scope.error.msg+="No. Faktur harus berawalan "+vf.wapu+"\n";
                    $scope.error.status+=1;
                }
                if(nama!=pbkk){
                    $scope.error.msg+="Nama PBKK seharusnya "+vf.pbkk+"\n";
                    $scope.error.status+=1;
                }
                if(npwp!=vf.npwp){
                    $scope.error.msg+="NPWP PBKK Seharusnya " +vf.npwp+"\n";
                    $scope.error.status+=1;
                }

                /*hanya test */
                return $scope.error.status===0;
                /*validasi bulan dan tahun*/
                if(bulan!=vf.bulan){
                    $scope.error.msg+="Beda bulan antara Faktur : "+bulan+
                    "\ndengan bulan pengajuan : "+vf.bulan+"\n";
                    $scope.error.status+=1;
                }
                if(tahun!=vf.tahun){
                    $scope.error.msg+="Beda tahun antara Faktur : "+tahun+
                    "\ndengan tahun pengajuan : "+vf.tahun;
                    $scope.error.status+=1;
                }
            }

            $scope.init=function(){
                if($routeParams.pmh){
                    Pmh.GetData({id:$routeParams.pmh,join:'faktur',child:'gr'},'pmh')
                    .then(function(r){
                        $scope.fkt=r.data.faktur;
                        $scope.fkt_f=r.data.fkt_f;
                        $scope.gr=r.data.gr;
                        $scope.revisi_fkt=Func.RevCode(r.data.revcode_id).fkt;
                        $scope.setPdfObject();
                    });
                }
            }

            $scope.init();

    /* end controller */
    }]
});
