define(['ui/system/pmh','ui/system/sap', 'ui/system/helper'], function() {
    return [ '$scope', '$routeParams', 'Pmh','Sap','Helper',  function( $scope, $routeParams,Pmh, Sap, Helper) {

        $scope.sap={}

        $scope.pmh={}


        $scope.sendParking=function(){
            var jgr=JSON.stringify($scope.pmh.gr);
            var jpph=JSON.stringify($scope.pmh.pph);
            var tg=Date.parse($scope.sap.tgl).toString('yyyy-MM-dd');
            var q={
                catatan:$scope.catatan,
                po:$scope.pmh.po.po,
                tgl:tg,
                taxcode:$scope.pmh.taxcode,
                faktur:$scope.pmh.faktur.no,
                invoice:$scope.pmh.invoice.no,
                gr:jgr,
                pph:jpph
            }

            Sap.Send(q,'parking')
            .then(function(r){
                if(r.data!=0){
                    $scope.sap.parking=Helper.toNumber(r.data);
                    return Pmh.SaveData($scope.sap,'sap');
                }
                return {data:0};
            })
            .then(function(r){
                var d=Helper.toNumber(r.data);
                if(r.data!=0) {
                    console.log(r.data);
                    $scope.sap.id=r.data;
                    Pmh.SaveData({id:$scope.pmh.id,sap_id:r.data},'pmh');
                }
            });
        }
        $scope.$watch('sap.tgl',function(val){
            if(val) $scope.sap.tahun=Date.parse(val).toString('yyyy');
        });

        $scope.sendMiro=function(){
            var tg=Date.parse($scope.sap.tgl).toString('yyyy-MM-dd');
            Sap.Send({parking:$scope.sap.parking,tgl:tg},'miro')
            .then(function(r){
                if(r.data!=0){
                    $scope.sap.miro=Helper.toNumber(r.data);
                    Pmh.SaveData({id:$scope.sap.id,miro:$scope.sap.miro},'sap');
                }
            });
        }

        $scope.reverseMiro=function(){

        }

        $scope.sendFb65=function(){
            var tg=Date.parse($scope.sap.tgl).toString('yyyy-MM-dd');
            var dpp=Helper.toNumber($scope.pmh.faktur.dpp);
            var pmh_denda=parseFloat($scope.pmh.denda);
            var dd=Math.round(dpp*pmh_denda/100);

            Sap.Send({
                denda:dd,
                miro:$scope.sap.miro,
                catatan:$scope.sap.catatan,
                tgl:tg
            },'fb65')
            .then(function(r){
                if(r.data!=0) {
                    $scope.sap.fb65=Helper.toNumber(r.data);
                    Pmh.SaveData({id:$scope.sap.id,fb65:$scope.sap.fb65},'sap');
                }

            });
        }
        Pmh.GetData({id:$routeParams.pmh,join:'po,faktur,invoice,sap',child:'pph,gr'},'pmh')
        .then(function(r){
            //$scope.sap.tgl=Date.today();
            if(r.data.sap && r.data.sap.tgl){
                var tgl=Helper.SapToDbDate(r.data.sap.tgl);
                $scope.sap.tgl=Date.parse(tgl);
            }
            $scope.sap=r.data.sap;
            delete r.data.sap;
            $scope.pmh=r.data;
        });


    /* end controller */
}]});
