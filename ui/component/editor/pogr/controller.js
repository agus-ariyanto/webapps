define([ 'ui/system/pmh','ui/system/sap','ui/system/helper','ui/system/func'], function() {
    return [ '$scope','$auth','Pmh','Sap','Helper', 'Func',function(
              $scope , $auth , Pmh ,Sap, Helper, Func){

        $scope.cbpo=[];
        $scope.kat_id='';
        $scope.kat={};
        $scope.kat_list=[];
        $scope.po={};
        $scope.po_list=[];
        $scope.gr=[];
        $scope.error={
            status:0,
            msg:''
        }
        $scope.showgr=false;
        $scope.modified=false;
        /* daftar kategori combobox */
        Pmh.GetData({order:'id'},'kat').then(function(r){
            angular.forEach(r.data, function(val){
                var id=val.id;
                $scope.kat_list[id]=val;
            });
        });

        /* daftar po combolist */
        Pmh.GetData({ccid:$auth.user.ccid},'po').then(function(rr){
            $scope.po_list=rr.data;
            angular.forEach(rr.data,function(val){
                val.tgl=Helper.sapToDate(val.tgl);
                $scope.cbpo[val.po]=val;
            });

        });

        /* validasi po status, bila fully invoice po akan direject  */
        $scope.checkPO=function(val){
            $scope.showgr=false;
            Sap.Send({po:val},'postatus').then(function(r){
                if(r.data==0) {
                    $scope.error.msg='Error : 401 (Belum login),  Silahkan login, \natau Error : Tidak ada data PO terkirim, silahkan check nomor PO ';
                    $scope.error.status=1;
                    return false;
                }

                if(r.data>1) {
                    $scope.error.msg='Status PO adalah Fully Invoice, \nSilahkan Pilih PO lainnya';
                    $scope.error.status=1;
                    return false;
                }
                $scope.error={
                    msg:'',
                    status:0
                }
                $scope.showgr=true;
            });

        }


        $scope.selectKat=function(index){
            $scope.kat=$scope.kat_list[index];
            $scope.modified=true;
        }


        /* select Nomor po dan tampilkan gr Checkbox */
        $scope.checked=0;
        $scope.selectPo = function(val) {
            $scope.po=$scope.cbpo[val];
            $scope.gr=[];
            /* cek po status */
            $scope.checkPO(val);
            /* ambil data gr untuk po terkait
            * gr yang diambil adalah gr yang belum diberi flag
            */
            $scope.gr_total='';

            Pmh.GetData({ po:val,pmh_id:0 },'gr')
            .then(function(r){
                $scope.gr=[];
                var n={},nn='';
                angular.forEach(r.data, function(val,key){
                    n=val;
                    n.selected=false;
                    n.tgl=Helper.sapToDate(val.tgl);
                    nn=Helper.toNumber(val.nilai);
                    n.nilai=Helper.toCurrency(nn);
                    $scope.gr.push(n);
                });
                $scope.modified=true;
            });

        }

        /* jumlah gr untuk patokan DPP */
        $scope.gr_total='';
        $scope.totalGr=function(){
            var total=0;
            $scope.gr_total='';
            angular.forEach($scope.gr,function(val,key){
                if(val.selected)
                    total+=Helper.toNumber(val.nilai);
            });
            if(total>0) $scope.gr_total=Helper.toCurrency(total);
        }


        /* simpan ke global variabel data.js, belum ke server */
        $scope.selected_gr=[];
        $scope.check=function(){
            $scope.error.status=0;
            if(!$scope.kat.id){
                $scope.error.msg='Kategori belum dipilih';
                $scope.error.status=1;
                return false;
            }
            if(!$scope.po.id){
                $scope.error.msg='Po belum dipilih';
                $scope.error.status=1;
                return false;
            }
            if(!$scope.gr_total){
                $scope.error.msg='Gr belum dipilih';
                $scope.error.status=1;
                return false;
            }
            $scope.selected_gr=[];
            angular.forEach($scope.gr,function(val,key){
                if(val.selected) $scope.selected_gr.push(val);
            });

            return true;
        }

    /* end controller */
    }]
});
