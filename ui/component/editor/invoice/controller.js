define([ 'ui/system/pmh','ui/system/helper','ui/system/func'], function() {
    return [ '$scope','$auth','$routeParams','$window','Pmh','Helper','Func', function( $scope , $auth ,$routeParams, $window ,  Pmh , Helper, Func){

        $scope.inv={
            dpp:'0',
            ppn:'0'
        }

        $scope.inv_f='';
        $scope.error={
            msg:'',
            status:0
        }
        $scope.intdate=0;
        $scope.barcode='0000';
        $scope.rek=[];
        $scope.revisi_inv=false;

        Pmh.GetData({ccid:$auth.user.ccid},'rek').then(function(r){
            $scope.rek=r.data;
            $scope.rek_select=0;
            $scope.selectRek();
        });
        $scope.rek_select=0;
        $scope.selectRek=function(){
            var idx=$scope.rek_select;
            $scope.inv.rek=$scope.rek[idx].rek;
            $scope.inv.bank=$scope.rek[idx].bank;
            $scope.inv.atasnama=$scope.rek[idx].an;
        }

        /* hitung jasa/material
        * dipaksa nilai jasa+material=dpp
        * karena hanya dua subject tersebut
        * yang jadi standar item invoice
        */
        var hitungTotal = function(isMaterial) {
            var total,
            subtotal = Helper.toNumber($scope.inv.dpp),
            ppn = Helper.toNumber($scope.inv.ppn),
            material,
            jasa;
            if (isMaterial) {
                material = Helper.toNumber($scope.inv.material);
                jasa = subtotal - material;
            } else {
                jasa = Helper.toNumber($scope.inv.jasa);
                material = subtotal - jasa;
            }
            total = subtotal + ppn;
            $scope.inv.jasa = Helper.toCurrency(jasa);
            $scope.inv.material = Helper.toCurrency(material);
            //$scope.inv.total = Helper.toCurrency(total);

            //var tbl = Helper.terbilang(total) + ' Rupiah';
            //$scope.inv.terbilang=tbl.substr(1,tbl.length);

        }

        $scope.$watch('inv.jasa',function(){
            hitungTotal(false);
        });
        $scope.$watch('inv.material',function(){
            hitungTotal(true);
        });

        /* manipulasi date-picker */
        $scope.tgl={
            inv:'',
            jatuhtempo:''
        }
        $scope.$watch('tgl.inv',function(val){
            if(val) $scope.inv.tgl = Date.parse(val).toString('dd/MM/yyyy');
        });
        $scope.$watch('tgl.jatuhtempo',function(val){
            if(val) $scope.inv.jatuhtempo = Date.parse(val).toString('dd/MM/yyyy');
        });


        /* barcode */
        $scope.bc = {
            format: 'CODE128',
            height: 40,
            lineColor: '#000000',
            width: 1,
            displayValue: true,
            textAlign: 'center',
            textPosition: 'bottom',
            textMargin: 2,
            fontSize: 12,
        }

        $scope.$watch('bc', function () {
            for (var key in $scope.bc) {
                if ($scope.bc.hasOwnProperty(key)) {
                    switch (typeof $scope.bc[key]) {
                        case 'number':
                        if ($scope.bc[key] == null) {
                            delete $scope.bc[key];
                        }
                        break;
                        case 'string':
                        if ($scope.bc[key].length < 1) {
                            delete $scope.bc[key];
                        }
                        break;
                        case 'boolean':
                        break;
                    }
                }
            }
        }, true);

        $scope.$watch('inv.pekerjaan',function(val){
            if(val) $scope.inv.pekerjaan=val.replace(/[\'\"]/g,' ');
        });

        /* cetak dan simpan data invoice  */

        $scope.check=function(){

            $scope.error={
                msg:'',
                status:0
            }

            if($scope.inv.tgl==''){
                $scope.msg+='Tanggal belum terisi\n';
                $scope.status+=1;
            }
            if($scope.inv.jatuhtempo==''){
                $scope.msg+='Tanggal Jatuh Tempo belum terisi\n';
                $scope.status+=1;
            }
            if($scope.inv.atasnama==''){
                $scope.msg+='Rekening Atas Nama belum terisi\n';
                $scope.status+=1;
            }
            if($scope.inv.nama==''){
                $scope.msg+='Nama Penanda Tangan belum terisi\n';
                $scope.status+=1;
            }

            if($scope.inv.jabatan==''){
                $scope.msg+='Jabatan Penanda Tangan belum terisi\n';
                $scope.status+=1;
            }
            var b=$scope.error.status==0;
            return b;
        }

        $scope.invoicePrint=function(){
            if(!$scope.check()){
                $window.alert('Belum bisa mencetak, \nlihat catatan kesalahan');
                return false;
            }
            $scope.modified=true;
            $window.print();
        }


        $scope.inv.printed=false;

        /* upload invoice */
        $scope.invoiceOnchange=function(el){

            var f=el.files[0];
            Pmh.Upload({upload:f},'inv/'+$auth.user.username).then(function(r){
                $scope.inv_f=r.data;
                $scope.revisi_inv=false;
                $scope.modified=true;
                $scope.setPdfObject();
            });
        }
        $scope.setPdfObject=function(){
            if($scope.inv_f && $scope.inv_f!='')
            Helper.createPdfObject($scope.inv_f,'invoice-object',56);
        }


        $scope.init=function(){
            if($routeParams.pmh){
                Pmh.GetData({id:$routeParams.pmh,join:'invoice',child:'gr'},'pmh')
                .then(function(r){
                    $scope.inv=r.data.invoice;
                    if(r.data.inv_f) $scope.inv_f=r.data.inv_f;
                    $scope.barcode=r.data.barcode;
                    //$scope.gr=r.data.gr;
                    $scope.revisi_inv=Func.RevCode(r.data.revcode_id).inv;
                    $scope.tgl.inv=$scope.inv.tgl;
                    $scope.tgl.jatuhtempo=$scope.inv.jatuhtempo;
                    $scope.setPdfObject();
                });
            }
        }

        $scope.init();

    /* end controller */
    }]
});
