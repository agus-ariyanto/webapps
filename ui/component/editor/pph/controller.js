define(['ui/system/pmh','ui/system/sap', 'ui/system/helper','ui/system/func'],function() {
    return [ '$scope', '$routeParams', '$auth','$window', 'Pmh','Sap', 'Helper', 'Func',
    function( $scope, $routeParams, $auth, $window, Pmh, Sap, Helper, Func) {

        $scope.pph=[];
        $scope.total_pph={
            currency:'',
            number:0
        }



        $scope.calcTotal=function(idx){
            var nominal=Helper.toNumber($scope.pph[idx].nominal_currency);
            var persen=parseFloat($scope.pph[idx].persen);
            var total=Math.round(persen*nominal/100);
            $scope.pph[idx].nominal=nominal;
            $scope.pph[idx].total=total;
            $scope.pph[idx].nominal_currency=Helper.toCurrency(nominal);
            $scope.pph[idx].total_currency=Helper.toCurrency(total);
        }

        Pmh.GetData({id:$routeParams.pmh,child:'pph'},'pmh')
        .then(function(r){
            var p=r.data.pph,c=r.data.ccid;

            if(p.length>0) return {data:p};
            return Sap.Send({ccid:c},'pph');
            //return {data:p};
        })
        .then(function(r){
            $scope.pph=Func.DataPph(r.data);
            $scope.total_pph=Func.TotalPph;
        });
    /* end controller */
}]});
