define([ 'ui/system/pmh','ui/system/helper','ui/system/func'], function() {
    return [ '$scope','$routeParams','$auth','Pmh','Helper','Func', function( $scope, $routeParams,$auth, Pmh , Helper,Func){
        $scope.ba_f='';
        $scope.pogr_f='';
        $scope.dpl_f='';
        $scope.error={
            status:0,
            msg:''
        }
        $scope.modified={
            ba:false,
            pogr:false,
            dpl:false
        }
        $scope.revisi={
            ba:false,
            pogr:false,
            dpl:false
        }
        $scope.showbtn={
            ba:true,
            pogr:true,
            dpl:true
        }
        $scope.revisiMode=function(){
            $scope.showbtn=$scope.revisi;
        }

        $scope.setPdfObject=function(){
            if($scope.ba_f && $scope.ba_f!='')
                Helper.createPdfObject($scope.ba_f,'ba-object',32);

            if($scope.dpl_f && $scope.dpl_f!='')
                Helper.createPdfObject($scope.dpl_f,'dpl-object',32);

            if($scope.pogr_f && $scope.pogr_f!='')
                Helper.createPdfObject($scope.pogr_f,'pogr-object',32);
        }

        /* unggah bap */
        $scope.baOnchange=function(el){
            var f=el.files[0];
            Pmh.Upload({ upload:f},'ba/'+$auth.user.username ).then(function(r){
                var d=r.data;
                $scope.ba_f=r.data;
                $scope.revisi.ba=false;
                $scope.modified.ba=true;
                $scope.setPdfObject();
            });
        }

        /* unggah pogr */
        $scope.pogrOnchange=function(el){
            var f=el.files[0];
            Pmh.Upload({upload:f},'pogr/'+$auth.user.username ).then(function(r){
                $scope.pogr_f=r.data;
                $scope.revisi.pogr=false;
                $scope.modified.pogr=true;
                $scope.setPdfObject();
            });
        }

        /* unggah dpl */
        $scope.dplOnchange=function(el){
            var f=el.files[0];
            Pmh.Upload({ upload:f },'dpl/'+$auth.user.username ).then(function(r){
                $scope.dpl_f=r.data;
                $scope.revisi.dpl=false;
                $scope.modified.dpl=true;
                $scope.setPdfObject();
            });
        }


        $scope.check=function(){
            $scope.error={
                msg:'',
                status:0
            }
            if(!$scope.ba_f){
                $scope.error.msg='Dokumen Berita Acara belum diunggah\n';
                $scope.error.status+=1;
            }
            if(!$scope.pogr_f){
                $scope.error.msg+='Dokumen Po dan Gr belum diunggah\n';
                $scope.error.status+=1;
            }
            if(!$scope.dpl_f){
                $scope.error.msg='Dokumen Pendukung Lainnya belum diunggah';
                $scope.error.status+=1;
            }
            return $scope.error.status===0;
        }


        $scope.init=function(){
            if($routeParams.pmh){
                Pmh.GetData({id:$routeParams.pmh},'pmh').then(function(r){
                    if(r.data.ba_f) $scope.ba_f=r.data.ba_f;
                    if(r.data.pogr_f) $scope.pogr_f=r.data.pogr_f;
                    if(r.data.dpl_f) $scope.dpl_f=r.data.dpl_f;
                    $scope.revisi=Func.RevCode(r.data.revcode_id);
                    $scope.setPdfObject();
                });
            }
        }

        $scope.init();

  /* end controller */
      }]
});
